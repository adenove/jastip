import 'package:flutter/material.dart';

class ButtonList extends StatelessWidget {
  final Color color;
  final Color colorText;
  final Function tap;
  final String name;
  final EdgeInsets padding;
  final EdgeInsets margin;

  ButtonList({
    this.tap,
    this.color,
    this.colorText,
    this.name = "",
    this.padding,
    this.margin,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin == null
          ? EdgeInsets.symmetric(horizontal: 10, vertical: 10)
          : margin,
      child: GestureDetector(
        onTap: tap,
        child: Container(
          padding: padding == null
              ? EdgeInsets.symmetric(horizontal: 15, vertical: 10)
              : padding,
          decoration: BoxDecoration(
            color: color == null ? Theme.of(context).accentColor : color,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                spreadRadius: 1,
                blurRadius: 6,
                offset: Offset(3, 4), // changes position of shadow
              ),
            ],
          ),
          child: Text(
            name,
            style: TextStyle(
              color: colorText == null ? Colors.white : colorText,
              fontWeight: FontWeight.w600,
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }
}
