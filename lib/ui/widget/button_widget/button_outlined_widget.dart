import 'package:flutter/material.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class ButtonOutlined extends StatelessWidget {
  final String title;
  final Function tap;
  final EdgeInsets padding;
  final Color color;
  final Color textColor;
  final double radius;
  final Color borderColor;
  final double borderWidth;
  final bool bold;
  final bool upperCase;
  final double spacing;
  final double textSize;

  ButtonOutlined({
    Key key,
    this.title = "",
    this.tap,
    this.padding = const EdgeInsets.symmetric(
      horizontal: 20,
      vertical: 15,
    ),
    this.color,
    this.textColor = Colors.black,
    this.upperCase = true,
    this.radius = 27,
    this.borderColor,
    this.borderWidth = 2,
    this.bold = false,
    this.spacing = 2,
    this.textSize = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: Container(
        padding: padding,
        decoration: BoxDecoration(
          color: color == null ? Colors.white : color,
          borderRadius: BorderRadius.circular(radius),
          border: Border.all(
            width: borderWidth,
            color: borderColor == null
                ? Theme.of(context).accentColor
                : borderColor,
          ),
        ),
        child: TextCustom(
          title: title,
          uppercase: true,
          color: textColor,
          align: TextAlign.center,
          bold: bold,
          textSize: textSize,
        ),
      ),
    );
  }
}
