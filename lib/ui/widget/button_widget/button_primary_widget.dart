import 'package:flutter/material.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class ButtonPrimary extends StatelessWidget {
  final String title;
  final Function tap;
  final EdgeInsets padding;
  final int notif;
  final Color color;
  final Color textColor;
  final double radius;
  final bool shadow;
  final bool bold;
  final bool upperCase;
  final double spacing;
  final double textSize;

  ButtonPrimary({
    Key key,
    this.title = "",
    this.tap,
    this.notif,
    this.padding = const EdgeInsets.symmetric(
      horizontal: 20,
      vertical: 15,
    ),
    this.color,
    this.textColor = Colors.white,
    this.upperCase = true,
    this.radius = 27,
    this.shadow = true,
    this.bold = false,
    this.spacing = 2,
    this.textSize = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: Container(
        padding: padding,
        decoration: BoxDecoration(
          color: color == null ? Theme.of(context).accentColor : color,
          borderRadius: BorderRadius.circular(radius),
          boxShadow: shadow
              ? [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 5,
                    offset: Offset(0, 5), // changes position of shadow
                  )
                ]
              : null,
        ),
        child: _renderButton(),
      ),
    );
  }

  Widget _renderButton() {
    if (notif != null) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextCustom(
            title: title,
            uppercase: true,
            color: textColor,
            align: TextAlign.center,
            bold: bold,
            textSize: textSize,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(100),
            ),
            child: TextCustom(
              title: "$notif",
              uppercase: true,
              align: TextAlign.center,
              bold: bold,
              textSize: 14,
            ),
          ),
        ],
      );
    }

    return TextCustom(
      title: title,
      uppercase: true,
      color: textColor,
      align: TextAlign.center,
      bold: bold,
      textSize: textSize,
    );
  }
}
