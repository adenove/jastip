import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/ui/screen/ChatPage/chat_detail_view/chat_detail_view.dart';
import 'package:jastip/ui/screen/ProfilToko/profil_toko_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';

class UserProfil extends StatelessWidget {
  final bool userBtn;
  final UserModel user;

  UserProfil({this.user, this.userBtn = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            width: 2,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: user.img != null
                ? Image.network(
                    user.img,
                    width: userBtn ? 80 : 120,
                    height: userBtn ? 80 : 120,
                    fit: BoxFit.cover,
                  )
                : Image.asset(
                    AssetsImg.noProfil,
                    width: userBtn ? 80 : 120,
                    height: userBtn ? 80 : 120,
                    fit: BoxFit.cover,
                  ),
          ),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                user.name == null ? user.email : user.name,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: userBtn ? 20 : 24,
                ),
              ),
              SizedBox(height: 5),
              Text(
                "${user.address}",
                style: TextStyle(
                  fontSize: userBtn ? 16 : 18,
                ),
              ),
              if (userBtn)
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      ButtonPrimary(
                        tap: () {
                          Get.to(() => ChatDetailView(user: user));
                        },
                        title: "Chat",
                        textColor: Colors.white,
                        bold: true,
                        padding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 15,
                        ),
                      ),
                      SizedBox(width: 20),
                      ButtonPrimary(
                        tap: () {
                          Get.to(() => ProfilTokoView(user: user));
                        },
                        title: "Visit",
                        textColor: Colors.white,
                        bold: true,
                        padding: EdgeInsets.symmetric(
                          vertical: 5,
                          horizontal: 15,
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          )
        ],
      ),
    );
  }
}
