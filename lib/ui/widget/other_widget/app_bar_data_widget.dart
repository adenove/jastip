import 'package:flutter/material.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/ui/widget/image_widget/image_radius_widget.dart';

class AppBarData extends StatelessWidget {
  final String title;
  final bool favorite;
  final Function trash;
  final bool chat;
  final Function tap;
  final UserModel user;

  AppBarData({
    this.title,
    this.favorite = false,
    this.trash,
    this.chat = false,
    this.tap,
    this.user,
  });

  Widget appData() {
    return Expanded(
      child: Row(
        children: [
          chat && user != null
              ? Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: ImageRadius(
                          img: user.img,
                          width: 50,
                          height: 50,
                        ),
                      ),
                    ),
                    Text(
                      user.name != null
                          ? user.name.toUpperCase()
                          : user.email.toUpperCase(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1,
                      ),
                    )
                  ],
                )
              : SizedBox(),
          if (title != null && user == null)
            Text(
              title.toUpperCase(),
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                letterSpacing: 1,
              ),
            )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: Row(
        children: [
          //back
          GestureDetector(
            onTap: () {
              tap();
            },
            child: Icon(
              Icons.arrow_back_ios_outlined,
              size: 20,
            ),
          ),
          SizedBox(width: 10),

          // title
          appData(),

          //delete
          if (trash != null)
            GestureDetector(
              onTap: trash,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: Icon(
                  Icons.delete,
                  color: Colors.grey,
                ),
              ),
            ),

          // favorite
          if (favorite)
            Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              decoration: BoxDecoration(
                color: Colors.grey,
                borderRadius: BorderRadius.circular(100),
              ),
              child: Icon(
                Icons.favorite,
                color: Colors.white,
                size: 15,
              ),
            )
        ],
      ),
    );
  }
}
