import 'package:flutter/material.dart';

class TitleText extends StatelessWidget {
  final String name;

  TitleText({this.name});

  @override
  Widget build(BuildContext context) {
    return Text(
      name.toUpperCase(),
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 19,
        letterSpacing: 1.8,
      ),
    );
  }
}
