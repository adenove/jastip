import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/binding/buyer_binding.dart';
import 'package:jastip/core/binding/seller_binding.dart';
import 'package:jastip/core/controller/sidebar_controller.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/ChatPage/chat_view/chat_view.dart';
import 'package:jastip/ui/screen/Home/Home_view.dart';
import 'package:jastip/ui/screen/Order/order_view.dart';
import 'package:jastip/ui/screen/Profil/profil_view.dart';
import 'package:jastip/ui/screen/Seller/seller_view.dart';
import 'package:jastip/ui/screen/Setting/setting_view.dart';
import 'package:jastip/ui/screen/Splash/splash_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_radius_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';

class Sidebar extends GetView<SidebarController> {
  final bool seller;

  Sidebar({this.seller = false});

  @override
  Widget build(BuildContext context) {
    Get.put(SidebarController());
    return ListView(
      children: [
        Container(
          width: double.infinity,
          child: Obx(() {
            if (controller.userDone) {
              return SizedBox(
                height: 200,
                child: WaitingIndicator(),
              );
            }

            return Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: ImageRadius(img: controller.user.img),
                ),
                SizedBox(height: 20),
                Text(
                  controller.user.name == null
                      ? "${controller.user.email}"
                      : "${controller.user.name}",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                  textAlign: TextAlign.center,
                ),
              ],
            );
          }),
        ),

        //list Sidebar
        lisItem(),

        Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child: Column(
            children: [
              ListNav(
                title: "Logout",
                tap: () {
                  AuthService.logout();
                  Get.reset();
                  Get.offAll(() => SplashView());
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget lisItem() {
    if (!seller) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            ListNav(
              title: "Profil",
              tap: () {
                Get.to(() => ProfilView());
              },
            ),
            ListNav(
              title: "Setting",
              tap: () {
                Get.to(() => SettingView());
              },
            ),
            ListNav(
              title: "My Order",
              tap: () => Get.to(() => OrderView()),
            ),
            GetX<SidebarController>(
              builder: (c) {
                return ListNav(
                  title: "Chat",
                  notif: c.countChat.value,
                  tap: () {
                    Get.to(() => Chat());
                  },
                );
              },
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              child: ButtonPrimary(
                tap: () {
                  Get.offAll(
                    () => SellerView(),
                    binding: SellerBinding(),
                  );
                },
                title: "Switch Seller Account",
                textColor: Colors.white,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                bold: true,
                textSize: 16,
              ),
            ),
          ],
        ),
      );
    } else {
      return Column(
        children: [
          SizedBox(height: 20),
          GetX<SidebarController>(
            builder: (c) {
              return Container(
                padding: EdgeInsets.all(20),
                child: ListNav(
                  title: "Chat",
                  notif: c.countChat.value,
                  tap: () {
                    Get.to(() => Chat());
                  },
                ),
              );
            },
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            child: ButtonPrimary(
              tap: () {
                Get.offAll(() => HomeView(), binding: BuyerBinding());
              },
              title: "Switch Buyer Account",
              textColor: Colors.white,
              padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
              bold: true,
              textSize: 16,
            ),
          ),
        ],
      );
    }
  }
}

class ListNav extends StatelessWidget {
  final String title;
  final Function tap;
  final int notif;

  ListNav({this.title, this.tap, this.notif = 0});

  @override
  Widget build(BuildContext context) {
    Widget notifBar() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Text(
          notif.toString(),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }

    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      child: ListTile(
        onTap: tap,
        title: Text(
          title,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        trailing: notif == 0 ? null : notifBar(),
      ),
    );
  }
}
