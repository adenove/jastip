import 'package:flutter/material.dart';

import 'text_custom_widget.dart';

class TextInfo extends StatelessWidget {
  final String title;
  final String value;

  const TextInfo({Key key, this.title, this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextCustom(
            title: "$title",
            color: Colors.grey,
            textSize: 12,
          ),
          SizedBox(height: 5),
          TextCustom(
            title: "$value",
            textSize: 18,
            bold: true,
          ),
        ],
      ),
    );
  }
}
