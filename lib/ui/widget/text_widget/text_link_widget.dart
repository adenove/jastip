import 'package:flutter/material.dart';

import 'text_custom_widget.dart';

class TextLink extends StatelessWidget {
  final Function tap;
  final String title;

  const TextLink({
    Key key,
    this.title = "",
    this.tap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: TextCustom(
        title: title,
        color: Theme.of(context).highlightColor,
        bold: true,
      ),
    );
  }
}
