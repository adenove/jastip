import 'package:flutter/material.dart';

class TextCustom extends StatelessWidget {
  final String title;
  final Color color;
  final bool bold;
  final bool uppercase;
  final TextAlign align;
  final double textSize;

  const TextCustom({
    Key key,
    this.title = "",
    this.color = Colors.black,
    this.bold = false,
    this.uppercase = false,
    this.align = TextAlign.start,
    this.textSize = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      uppercase ? title.toUpperCase() : title,
      style: TextStyle(
        color: color,
        fontWeight: bold ? FontWeight.bold : FontWeight.normal,
        fontSize: textSize,
      ),
      textAlign: align,
    );
  }
}
