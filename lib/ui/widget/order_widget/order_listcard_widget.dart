import 'package:flutter/material.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class OrderListCardWidget extends StatelessWidget {
  final OrderDataModel order;
  final Function tap;
  final Function longPress;
  final bool selected;

  OrderListCardWidget({
    Key key,
    this.order,
    this.tap,
    this.longPress,
    this.selected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      onLongPress: longPress,
      child: Card(
        color: selected ? Colors.blue : Colors.white,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              TextCustom(
                title: "${order.createdAt.toDate()}",
                textSize: 12,
                align: TextAlign.end,
              ),
              SizedBox(height: 5),
              TextCustom(
                  title:
                      "Pembeli : ${order.userOrder.name != null ? order.userOrder.name : order.userOrder.email}"),
              TextCustom(title: "Address : ${order.userOrder.address}"),
              TextCustom(
                  title:
                      "Penjual : ${order.userProduct.name != null ? order.userProduct.name : order.userProduct.email}"),
              TextCustom(title: "Address : ${order.userProduct.address}"),
              TextCustom(title: "Product : ${order.product.idCode}"),
              status(),
            ],
          ),
        ),
      ),
    );
  }

  Widget status() {
    if (order.status == -1) {
      return Row(
        children: [
          TextCustom(
            title: "Status : ",
          ),
          TextCustom(
            title: "Order Dibatalkan ",
            color: Colors.red,
          ),
        ],
      );
    }

    if (order.status == 0) {
      return Row(
        children: [
          TextCustom(
            title: "Status : ",
          ),
          TextCustom(
            title: "Menunggu Konfirmasi ",
            color: Colors.green,
          ),
        ],
      );
    }

    if (order.status == 1) {
      return Row(
        children: [
          TextCustom(
            title: "Status : ",
          ),
          TextCustom(
            title: "DiKonfirmasi",
            color: Colors.green,
          ),
        ],
      );
    }

    if (order.status == 2) {
      return Row(
        children: [
          TextCustom(
            title: "Status : ",
          ),
          TextCustom(
            title: "Order Selesai",
            color: Colors.green,
          ),
        ],
      );
    }

    return TextCustom(
      title: "Status : Tidak Jelas",
      color: Colors.pink,
    );
  }
}
