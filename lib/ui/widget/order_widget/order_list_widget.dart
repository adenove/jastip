import 'package:flutter/material.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/ui/widget/image_widget/image_square_picker_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class OrderList extends StatelessWidget {
  final OrderDataModel data;
  final bool widgetStatus;
  final Function tap;

  const OrderList({
    Key key,
    this.data,
    this.widgetStatus = true,
    this.tap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        tap();
      },
      child: Card(
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 20,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _orderStatus(data.status),
                  TextCustom(
                    title:
                        "${data.userOrder.name == null ? data.userOrder.email : data.userOrder.name}",
                    textSize: 14,
                    color: Colors.grey,
                  ),
                ],
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  ImageSquare(
                    img: data.product.img,
                    height: 80,
                    width: 80,
                    picker: false,
                  ),
                  SizedBox(width: 20),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        TextCustom(
                          title:
                              "${data.userProduct.name == null ? data.userProduct.email : data.userProduct.name}",
                          bold: true,
                        ),
                        SizedBox(height: 5),
                        TextCustom(
                          title: "${data.product.name}",
                          bold: true,
                          textSize: 14,
                        ),
                        SizedBox(height: 5),
                        Row(
                          children: [
                            TextCustom(
                              title: "Jumlah : ",
                              color: Colors.grey,
                              bold: true,
                              textSize: 14,
                            ),
                            TextCustom(
                              title: "${data.count}",
                              color: Colors.grey,
                              textSize: 14,
                              bold: true,
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        TextCustom(
                          title: "${data.createdAt.toDate()}",
                          textSize: 12,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              if (data.status == 1)
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 4),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Theme.of(context).accentColor,
                      ),
                      child: TextCustom(
                        title: "Selesaikan Transaksi",
                        color: Colors.white,
                        bold: true,
                        textSize: 14,
                      ),
                    ),
                  ],
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _orderStatus(int status) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: status < 0
            ? Colors.red
            : status == 0
                ? Colors.yellow
                : Colors.green,
      ),
      child: TextCustom(
        title:
            "${status < 0 ? "Pesanan Dibatalkan" : status == 0 ? "Menunggu Konfirmasi" : status == 1 ? "Pesanan Terkonfirmasi" : "Pesanan Selesai"}",
        color: status == 0 ? Colors.black : Colors.white,
        bold: true,
        textSize: 12,
      ),
    );
  }
}
