import 'dart:io';
import 'package:flutter/material.dart';

class ImageFile extends StatelessWidget {
  final File img;
  final double width;
  final double height;
  final double radius;
  final bool local;

  const ImageFile({
    Key key,
    this.img,
    this.width = 150,
    this.height = 150,
    this.radius = 100,
    this.local = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: Image.file(
        img,
        height: width,
        width: height,
        fit: BoxFit.cover,
      ),
    );
  }
}
