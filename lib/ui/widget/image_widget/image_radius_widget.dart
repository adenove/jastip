import 'package:flutter/material.dart';
import 'package:jastip/config/assets_config.dart';

class ImageRadius extends StatelessWidget {
  final String img;
  final double width;
  final double height;
  final double radius;
  final bool local;

  const ImageRadius({
    Key key,
    this.img,
    this.width = 150,
    this.height = 150,
    this.radius = 100,
    this.local = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(radius),
      child: img != null && !local
          ? Image.network(
              img,
              height: width,
              width: height,
              fit: BoxFit.cover,
            )
          : Image.asset(
              img == null ? AssetsImg.noProfil : img,
              height: width,
              width: height,
              fit: BoxFit.cover,
            ),
    );
  }
}
