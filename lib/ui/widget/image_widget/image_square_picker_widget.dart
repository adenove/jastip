import 'dart:io';

import 'package:flutter/material.dart';
import 'package:jastip/config/assets_config.dart';

class ImageSquare extends StatelessWidget {
  final String img;
  final File file;
  final Function tap;
  final double height;
  final double width;
  final bool picker;

  const ImageSquare({
    Key key,
    this.file,
    this.img,
    this.tap,
    this.height = 250,
    this.width = double.infinity,
    this.picker = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: Container(
        color: Colors.grey[200],
        child: Stack(
          children: [
            _imgView(),
            if (picker)
              Positioned(
                right: 20,
                top: 20,
                child: Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: Icon(
                    Icons.add_a_photo,
                    color: Colors.white,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Widget _imgView() {
    if (file != null) {
      return Image.file(
        file,
        width: width,
        height: height,
        fit: BoxFit.contain,
      );
    }

    if (img != null) {
      return Image.network(
        img,
        width: width,
        height: height,
        fit: BoxFit.contain,
      );
    }

    return Image.asset(
      AssetsImg.noImg,
      width: width,
      height: height,
      fit: BoxFit.contain,
    );
  }
}
