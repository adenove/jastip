import 'package:flutter/material.dart';
import 'package:jastip/ui/widget/image_widget/image_square_picker_widget.dart';

class ImageProduct extends StatelessWidget {
  final String img;
  const ImageProduct({Key key, this.img}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      child: ImageSquare(img: img),
    );
  }
}
