import 'dart:io';

import 'package:flutter/material.dart';
import 'package:jastip/ui/widget/image_widget/image_file_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_radius_widget.dart';

class ImagePicker extends StatelessWidget {
  final String img;
  final Function tap;
  final File file;

  const ImagePicker({
    Key key,
    this.img,
    this.tap,
    this.file,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: Container(
        child: Stack(
          children: [
            file == null
                ? ImageRadius(
                    img: img,
                    width: 200,
                    height: 200,
                  )
                : ImageFile(
                    img: file,
                    width: 200,
                    height: 200,
                  ),
            Positioned(
              right: 0,
              top: 20,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Icon(
                  Icons.edit,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
