import 'package:flutter/material.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/config/helper.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';

class ProductListCard extends StatelessWidget {
  final ProductModel data;
  final bool isYou;
  final Function edit;
  final Function delete;
  final Function buy;

  ProductListCard({
    this.data,
    this.isYou = false,
    this.edit,
    this.delete,
    this.buy,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 40),
      child: Row(
        children: [
          data.img != null
              ? Image.network(
                  data.img,
                  height: 200,
                  width: 150,
                  fit: BoxFit.cover,
                )
              : Image.asset(
                  AssetsImg.noImg,
                  height: 200,
                  width: 150,
                  fit: BoxFit.cover,
                ),
          SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                listText("ID : ${data.idCode}"),
                listText("Date : ${data.createdAt.toDate()}"),
                listText("Stock : ${data.stock}"),
                listText("Location : ${data.location}"),
                listText("Price : ${Helper.rupiah(data.price)}"),
                SizedBox(height: 30),
                !isYou
                    ? ButtonPrimary(
                        tap: buy,
                        title: "Buy",
                        bold: true,
                        textColor: Colors.white,
                        padding:
                            EdgeInsets.symmetric(vertical: 1, horizontal: 20),
                      )
                    : Row(
                        children: [
                          ButtonPrimary(
                            tap: () => delete(data.id),
                            title: "Delete",
                            bold: true,
                            textColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                vertical: 1, horizontal: 10),
                          ),
                          SizedBox(width: 10),
                          ButtonPrimary(
                            tap: () => edit(data.id),
                            title: "Edit",
                            bold: true,
                            textColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                              vertical: 1,
                              horizontal: 10,
                            ),
                          )
                        ],
                      )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget listText(String value) {
    return Text(
      "$value",
      style: TextStyle(
        fontSize: 16,
      ),
    );
  }
}
