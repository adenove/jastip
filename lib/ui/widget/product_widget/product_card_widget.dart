import 'package:flutter/material.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/config/helper.dart';

class ProductCard extends StatelessWidget {
  final ProductModel product;
  final Function tap;

  ProductCard({this.product, this.tap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: tap,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: product.img != null
                  ? Image.network(
                      product.img,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    )
                  : Image.asset(
                      AssetsImg.noImg,
                      width: double.infinity,
                      fit: BoxFit.cover,
                    ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 5),
                  Text(
                    Helper.rupiah(product.price),
                    maxLines: 1,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
