import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/ui/screen/Admin/page/feed/feed_admin_controller.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class FeedCard extends StatelessWidget {
  final FeedAdminController controller = Get.find();
  final ProductModel product;
  final Function tap;
  final Function longPress;

  FeedCard({
    Key key,
    this.tap,
    this.product,
    this.longPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      bool clicked = controller.selectsFeed.contains(product.id);
      return GestureDetector(
        onTap: () => controller.edit(product.id),
        onLongPress: () => controller.longPress(product.id),
        child: Card(
          color: clicked ? Colors.blue : Colors.white,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextCustom(title: "ID : ${product.idCode}"),
                TextCustom(title: "Name : ${product.name}"),
                TextCustom(title: "Loc : ${product.location}"),
                TextCustom(title: "Date : ${product.createdAt.toDate()}"),
              ],
            ),
          ),
        ),
      );
    });
  }
}

// class FeedCard extends StatefulWidget {
//   final ProductModel product;
//   final Function tap;
//   const FeedCard({Key key, this.tap, this.product}) : super(key: key);

//   @override
//   _FeedCardState createState() => _FeedCardState();
// }

// class _FeedCardState extends State<FeedCard> {
//   bool onpress = false;

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       onTap: () {
//         if (!onpress) {
//           widget.tap(widget.product.id);
//         } else {
//           setState(() {
//             onpress = false;
//           });
//         }
//       },
//       onLongPress: () {
//         setState(() {
//           onpress = true;
//         });
//       },
//       child: Card(
//         color: onpress ? Colors.blue : Colors.white,
//         child: Container(
//           padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               TextCustom(title: "ID : ${widget.product.idCode}"),
//               TextCustom(title: "Name : ${widget.product.name}"),
//               TextCustom(title: "Loc : ${widget.product.location}"),
//               TextCustom(title: "Date : ${widget.product.createdAt.toDate()}"),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
