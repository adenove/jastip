import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/config/helper.dart';
import 'package:jastip/core/controller/order_controller.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/order_sucess_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/user_profil_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';
import 'child/product_thumbnail_child_view.dart';

class OrderProductView extends GetView<OrderController> {
  final ProductModel data;

  const OrderProductView({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(OrderController(uid: data.idUser));
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(
              title: "Order",
              tap: Get.back,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: ListView(
                  children: [
                    ProductThumbnailView(data: data),
                    SizedBox(height: 20),
                    Obx(() {
                      if (controller.userEror) {
                        return TextCustom(
                          title: "Gagal Mengambil Data",
                          color: Colors.grey[300],
                          bold: true,
                        );
                      }

                      if (!controller.userDone) {
                        return UserProfil(user: controller.user, userBtn: true);
                      }

                      return WaitingIndicator();
                    }),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20),
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Note :",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "Pesan Untuk Penjual",
                          style: TextStyle(
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Total Harga :",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          Helper.rupiah(data.price),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 40),
                  ButtonPrimary(
                    tap: _buy,
                    title: "Buy",
                    textColor: Colors.white,
                    bold: true,
                    padding: EdgeInsets.symmetric(
                      vertical: 15,
                      horizontal: 120,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _buy() {
    Get.defaultDialog(
      title: data.name,
      content: orderDialog(),
    );
  }

  Widget orderDialog() {
    controller.countOrder(1);
    return Obx(() {
      if (!controller.buyWait) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonPrimary(
                    tap: () => controller.actionCount(-1),
                    title: "-",
                    bold: true,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: TextCustom(
                      title: "${controller.count}",
                      bold: true,
                    ),
                  ),
                  ButtonPrimary(
                    tap: () => controller.actionCount(1),
                    title: "+",
                    bold: true,
                    padding: EdgeInsets.symmetric(horizontal: 18, vertical: 4),
                  ),
                ],
              ),
              SizedBox(height: 20),
              ButtonPrimary(
                tap: () {
                  controller
                      .order(OrderModel(
                    idProduct: data.id,
                    idUserProduct: data.idUser,
                    idUserOrder: AuthService.auth.currentUser.uid,
                    count: controller.count,
                    status: 0,
                    createdAt: Timestamp.fromDate(DateTime.now()),
                  ))
                      .then((value) {
                    Get.to(() => OrderSucessView(user: controller.user));
                  }).catchError((e) {
                    print(e);
                  });
                },
                title: "Buy",
                padding: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
                bold: true,
              ),
            ],
          ),
        );
      }
      return WaitingIndicator();
    });
  }
}
