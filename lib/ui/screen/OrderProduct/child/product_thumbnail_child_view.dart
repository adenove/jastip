import 'package:flutter/material.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/config/helper.dart';
import 'package:jastip/ui/widget/image_widget/image_square_picker_widget.dart';

class ProductThumbnailView extends StatelessWidget {
  final ProductModel data;

  ProductThumbnailView({this.data});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
        border: Border.all(width: 1),
      ),
      child: Row(
        children: [
          // Image.network(
          //   data.img,
          //   width: 120,
          //   height: 120,
          //   fit: BoxFit.cover,
          // ),
          ImageSquare(
            img: data.img,
            width: 120,
            height: 120,
            picker: false,
          ),
          SizedBox(width: 20),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  data.name,
                  maxLines: 3,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  Helper.rupiah(data.price),
                  maxLines: 2,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
