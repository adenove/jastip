import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/core/controller/auth_controller.dart';
import 'package:jastip/ui/screen/Auth/register_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_link_widget.dart';

class LoginView extends GetView<AuthController> {
  final TextEditingController emailCtrl = TextEditingController();
  final TextEditingController passwordCtrl = TextEditingController();

  LoginView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: [
              Image.asset(
                AssetsImg.jastip,
                width: 150,
                height: 150,
              ),
              SizedBox(height: 40),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    TextField(
                      controller: emailCtrl,
                      decoration: InputDecoration(labelText: "Email"),
                    ),
                    SizedBox(height: 20),
                    Obx(
                      () => TextField(
                        controller: passwordCtrl,
                        obscureText: controller.scured.value,
                        decoration: InputDecoration(
                          labelText: "Password",
                          suffixIcon: IconButton(
                            onPressed: () => controller.scured.toggle(),
                            icon: Icon(
                              controller.scured.value
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 40),
                    Obx(() {
                      if (controller.isWait.value) {
                        return WaitingIndicator();
                      } else {
                        return ButtonPrimary(
                          tap: () => controller.login(
                              emailCtrl.text, passwordCtrl.text),
                          title: "Login",
                        );
                      }
                    }),
                    SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextCustom(
                          title: "don't have an account ?",
                          color: Colors.grey,
                        ),
                        SizedBox(width: 10),
                        TextLink(
                          tap: () => Get.to(() => RegisterView()),
                          title: "Register",
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
