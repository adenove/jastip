import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/user_controller.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_picker_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';

class SettingView extends GetView<UserController> {
  final TextEditingController nama = TextEditingController();
  final TextEditingController phone = TextEditingController();
  final TextEditingController address = TextEditingController();
  final TextEditingController email = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Get.put(UserController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(title: "Setting", tap: () => Get.back()),
            GetBuilder<UserController>(
              builder: (s) => Center(
                child: ImagePicker(
                  tap: controller.getImage,
                  img: controller.user.img,
                  file: s.fileProfil,
                  // local: controller.profilImg == "" ? false : true,
                ),
              ),
            ),
            Expanded(
              child: Obx(() {
                if (!controller.userDone) {
                  return ListView(
                    children: [
                      SizedBox(height: 20),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 40, horizontal: 40),
                        child: Column(
                          children: [
                            TextFormField(
                              onChanged: (e) => nama.text = e,
                              initialValue: controller.user.name,
                              decoration: InputDecoration(
                                hintText: "Nama",
                              ),
                            ),
                            SizedBox(height: 10),
                            TextFormField(
                              onChanged: (e) => phone.text = e,
                              initialValue: controller.user.phone,
                              decoration: InputDecoration(
                                hintText: "Phone",
                              ),
                            ),
                            SizedBox(height: 10),
                            TextFormField(
                              onChanged: (e) => email.text = e,
                              initialValue: controller.user.email,
                              decoration: InputDecoration(
                                hintText: "Email",
                              ),
                            ),
                            SizedBox(height: 10),
                            TextFormField(
                              onChanged: (e) => address.text = e,
                              initialValue: controller.user.address,
                              decoration: InputDecoration(
                                hintText: "Address",
                              ),
                            ),
                            SizedBox(height: 60),
                            Obx(() {
                              if (!controller.wait) {
                                return ButtonPrimary(
                                  tap: () {
                                    controller.updateProfil(
                                      name: nama.text,
                                      email: email.text,
                                      phone: phone.text,
                                      address: address.text,
                                      img: controller.fileProfil,
                                    );
                                  },
                                  title: "Update Profil",
                                  bold: true,
                                );
                              } else {
                                return WaitingIndicator();
                              }
                            }),
                          ],
                        ),
                      ),
                    ],
                  );
                }

                return WaitingIndicator();
              }),
            ),
          ],
        ),
      ),
    );
  }
}
