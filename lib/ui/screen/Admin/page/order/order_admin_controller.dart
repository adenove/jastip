import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class OrderAdminController extends GetxController {
  final orderRefresh = false.obs;
  final orderIsload = false.obs;
  final orderEror = false.obs;
  final statusRefresh = false.obs;
  final orders = List.filled(0, OrderDataModel(), growable: true).obs;
  var selects = [].obs;

  @override
  void onInit() {
    super.onInit();

    orders.bindStream(
      OrderServices.streamAllOrder().asyncMap((List<OrderModel> event) async {
        if (!orderIsload.value) orderRefresh(true);
        orderEror(false);
        return OrderServices.joinTable(event).then((value) {
          if (!orderIsload.value) orderRefresh(false);
          orderIsload(true);
          return value;
        }).catchError((e) {
          print(e);
          orderEror(true);
        });
      }),
    );
  }

  void longPress(String id) {
    if (!selects.contains(id)) {
      selects.add(id);
    }
  }

  void deleteOrder() async {
    orderIsload(false);
    await Future.forEach(selects, (e) async {
      await OrderServices.deleteOrder(e);
    }).then((value) {
      selects.clear();
    }).catchError((e) {
      print(e);
    });
  }
}
