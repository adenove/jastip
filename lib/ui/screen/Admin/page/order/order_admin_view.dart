import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/ui/screen/Admin/page/order/order_admin_controller.dart';
import 'package:jastip/ui/widget/order_widget/order_listcard_widget.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class OrderAdminView extends GetView<OrderAdminController> {
  const OrderAdminView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(OrderAdminController());
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Obx(
                () => AppBarData(
                  title: "Order",
                  tap: Get.back,
                  trash: controller.selects.length < 1
                      ? null
                      : controller.deleteOrder,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: GetX<OrderAdminController>(builder: (c) {
                    if (c.orderEror.value) {
                      return ListView(
                        children: [
                          SizedBox(height: Get.height / 2.2),
                          TextCustom(
                            title: "terjadi kesalahan",
                            bold: true,
                            color: Colors.grey,
                          ),
                        ],
                      );
                    }

                    if (!c.orderRefresh.value) {
                      if (c.orders.length < 1) {
                        return ListView(
                          children: [
                            SizedBox(height: Get.height / 2.2),
                            Center(
                              child: TextCustom(
                                title: "Order Masih kosong",
                                bold: true,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        );
                      }
                      return ListView.builder(
                        itemCount: c.orders.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Obx(
                            () => OrderListCardWidget(
                              longPress: () {
                                controller.longPress(c.orders[index].id);
                              },
                              selected: controller.selects.contains(
                                c.orders[index].id,
                              ),
                              tap: () {
                                if (c.selects.length > 0) {
                                  if (c.selects.contains(c.orders[index].id)) {
                                    controller.selects
                                        .remove(c.orders[index].id);
                                  } else {
                                    controller.selects.add(c.orders[index].id);
                                  }
                                }
                              },
                              order: c.orders[index],
                              // widgetStatus: false,
                            ),
                          );
                        },
                      );
                    }

                    return Center(child: WaitingIndicator());
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
