import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class TransaksiAdminController extends GetxController {
  final historyIsloaded = false.obs;
  final historyLoad = false.obs;
  final historyLoadEror = false.obs;
  final test = true.obs;
  final historys = List.filled(0, OrderDataModel(), growable: true).obs;
  final selects = [].obs;

  @override
  void onInit() {
    super.onInit();
    historys.bindStream(OrderServices.streamAllHistory().asyncMap((event) {
      if (!historyIsloaded.value) historyLoad(true);
      historyLoadEror(false);
      return OrderServices.joinTable(event).then((value) {
        historyIsloaded(true);
        historyLoad(false);
        return value;
      }).catchError((e) {
        historyLoadEror(true);
        print(e);
      });
    }));
  }

  void longPress(String id) async {
    if (!selects.contains(id)) {
      selects.add(id);
    }
  }

  void delete() async {
    historyIsloaded(false);
    await Future.forEach(selects, (e) async {
      await OrderServices.deleteOrder(e);
    }).then((value) {
      selects.clear();
    }).catchError((e) {
      print(e);
    });
  }
}
