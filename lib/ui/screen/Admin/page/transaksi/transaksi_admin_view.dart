import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/ui/screen/Admin/page/transaksi/transaksi_admin_controller.dart';
import 'package:jastip/ui/widget/order_widget/order_listcard_widget.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class TransaksiAdminView extends GetView<TransaksiAdminController> {
  const TransaksiAdminView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(TransaksiAdminController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Obx(
              () => AppBarData(
                title: "Transaksi",
                tap: Get.back,
                trash: controller.selects.length < 1 ? null : controller.delete,
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: GetX<TransaksiAdminController>(builder: (t) {
                  if (t.historyLoadEror.value) {
                    return ListView(
                      children: [
                        SizedBox(height: Get.height / 2.2),
                        Center(
                          child: TextCustom(
                            title: "terjadi kesalahan",
                            bold: true,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    );
                  }

                  if (!t.historyLoad.value) {
                    if (t.historys.length < 1) {
                      return ListView(
                        children: [
                          SizedBox(height: Get.height / 2.2),
                          Center(
                            child: TextCustom(
                              title: "History Masih kosong",
                              bold: true,
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      );
                    }

                    return ListView.builder(
                      itemCount: t.historys.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Obx(
                          () => OrderListCardWidget(
                            tap: () {
                              if (controller.selects.length > 0) {
                                if (!controller.selects
                                    .contains(t.historys[index].id)) {
                                  controller.selects.add(t.historys[index].id);
                                } else {
                                  controller.selects
                                      .remove(t.historys[index].id);
                                }
                              }
                            },
                            longPress: () => t.longPress(
                              t.historys[index].id,
                            ),
                            selected: controller.selects.contains(
                              controller.historys[index].id,
                            ),
                            order: t.historys[index],
                          ),
                        );
                      },
                    );
                  }

                  return Center(child: WaitingIndicator());
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
