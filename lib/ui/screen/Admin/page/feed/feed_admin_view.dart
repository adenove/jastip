import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/product_widget/feed_card_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';
import 'feed_admin_controller.dart';

class FeedAdminView extends GetView<FeedAdminController> {
  const FeedAdminView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(FeedAdminController());
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Obx(
                () => AppBarData(
                  title: "Feed",
                  tap: Get.back,
                  trash: controller.selectsFeed.length < 1
                      ? null
                      : () => controller.delete(),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Obx(() {
                    if (controller.feedEror) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            children: [
                              TextCustom(title: "Terjadi Ksalahan"),
                            ],
                          ),
                        ],
                      );
                    }

                    if (controller.feedWait) {
                      return Center(child: WaitingIndicator());
                    }

                    if (controller.feed.length < 1) {
                      return Center(
                        child: TextCustom(
                          title: "Feed Masih Kosong",
                          bold: true,
                          color: Colors.grey[400],
                        ),
                      );
                    }

                    return ListView.builder(
                      itemCount: controller.feed.length,
                      itemBuilder: (_, i) {
                        return FeedCard(
                          product: controller.feed[i],
                          // edit: controller.edit,
                          // delete: controller.delete,
                          // isYou: true,
                        );
                      },
                    );
                  }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
