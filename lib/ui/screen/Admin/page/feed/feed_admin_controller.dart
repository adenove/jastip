import 'package:get/get.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/firestore/product_services.dart';
import 'package:jastip/ui/screen/Seller/Page/product_manager_view.dart';

class FeedAdminController extends GetxController {
  var feed = List.filled(0, ProductModel(), growable: true).obs;
  var feedLoad = false.obs;
  var feedIsEror = false.obs;
  final selectsFeed = [].obs;

  get feedWait => feedLoad.value;
  get feedEror => feedIsEror.value;

  @override
  void onInit() {
    super.onInit();
    feed.bindStream(ProductServices.streamAllProudct());
  }

  void edit(String id) {
    if (selectsFeed.length > 0) {
      if (selectsFeed.contains(id)) {
        selectsFeed.remove(id);
      } else {
        selectsFeed.add(id);
      }

      return;
    }

    Get.to(
      () => ProductManagerView(
        idProduct: id,
        title: "Product",
        admin: true,
      ),
    );
  }

  void longPress(String id) {
    if (!selectsFeed.contains(id)) {
      selectsFeed.add(id);
    }
  }

  void delete() async {
    if (!feedLoad.value) {
      feedLoad(true);
      await Future.forEach(selectsFeed, (id) async {
        await ProductServices.deleteProduct(id).then((value) {
          selectsFeed.remove(id);
        }).catchError((e) {
          print("no-img");
        });
      }).then((value) {
        selectsFeed.clear();
        feedLoad(false);
      });
    }
  }
}
