import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/seller_controller.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/Admin/page/feed/feed_admin_view.dart';
import 'package:jastip/ui/screen/Admin/page/order/order_admin_view.dart';
import 'package:jastip/ui/screen/Admin/page/transaksi/transaksi_admin_view.dart';
import 'package:jastip/ui/screen/Splash/splash_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';

class AdminView extends GetView<SellerController> {
  const AdminView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(SellerController());
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: [
                ButtonPrimary(
                  tap: () {
                    Get.to(() => FeedAdminView());
                  },
                  textSize: 20,
                  bold: true,
                  title: "Feed",
                  radius: 100,
                  padding: EdgeInsets.symmetric(vertical: 30),
                ),
                SizedBox(height: 20),
                ButtonPrimary(
                  tap: () {
                    Get.to(() => OrderAdminView());
                  },
                  textSize: 20,
                  bold: true,
                  title: "Order",
                  radius: 100,
                  padding: EdgeInsets.symmetric(vertical: 30),
                ),
                SizedBox(height: 20),
                ButtonPrimary(
                  tap: () {
                    Get.to(() => TransaksiAdminView());
                  },
                  textSize: 20,
                  bold: true,
                  title: "Transaksi",
                  radius: 100,
                  padding: EdgeInsets.symmetric(vertical: 30),
                ),
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ButtonPrimary(
                      tap: () {
                        AuthService.logout();
                        Get.reset();
                        Get.offAll(() => SplashView());
                      },
                      padding: EdgeInsets.symmetric(
                        horizontal: 30,
                        vertical: 10,
                      ),
                      textSize: 20,
                      bold: true,
                      title: "Logout",
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
