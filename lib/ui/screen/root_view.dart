import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/binding/buyer_binding.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/Admin/admin_view.dart';
import 'package:jastip/ui/screen/Home/Home_view.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';

class RootView extends StatefulWidget {
  const RootView({Key key}) : super(key: key);

  @override
  _RootViewState createState() => _RootViewState();
}

class _RootViewState extends State<RootView> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (AuthService.auth.currentUser.uid == "SbQNz7PKdtaB13Gp25LCgOtDC5h2") {
        Get.offAll(AdminView(), binding: BuyerBinding());
      } else {
        Get.offAll(HomeView(), binding: BuyerBinding());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: WaitingIndicator(),
      ),
    );
  }
}
