import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/user_controller.dart';
import 'package:jastip/ui/screen/Setting/setting_view.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_radius_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_info_widget.dart';

class ProfilView extends GetView<UserController> {
  ProfilView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(UserController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(title: "Profil", tap: () => Get.back()),
            Expanded(
              child: Obx(() {
                if (!controller.userDone) {
                  return ListView(
                    children: [
                      Center(
                        child: ImageRadius(
                          img: controller.user.img,
                          width: 200,
                          height: 200,
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 40, horizontal: 40),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TextInfo(
                              title: "Nama",
                              value: "${controller.user.name}",
                            ),
                            SizedBox(height: 15),
                            TextInfo(
                              title: "Phone",
                              value: "${controller.user.phone}",
                            ),
                            SizedBox(height: 15),
                            TextInfo(
                              title: "Address",
                              value: "${controller.user.address}",
                            ),
                            SizedBox(height: 15),
                            TextInfo(
                              title: "Email",
                              value: "${controller.user.email}",
                            ),
                            SizedBox(height: 100),
                            Center(
                              child: ButtonPrimary(
                                tap: () => Get.to(() => SettingView()),
                                title: "Update Profil",
                                bold: true,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }
                return WaitingIndicator();
              }),
            ),
          ],
        ),
      ),
    );
  }
}
