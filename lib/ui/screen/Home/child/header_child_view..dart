import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:math' as math;
import 'package:jastip/core/controller/home_controller.dart';

class HeaderView extends StatelessWidget {
  final HomeController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () {
              Scaffold.of(context).openDrawer();
            },
            child: Transform(
              alignment: Alignment.center,
              transform: Matrix4.rotationY(math.pi),
              child: Icon(
                Icons.list_sharp,
                size: 35,
              ),
            ),
          ),
          SizedBox(width: 10),
          Expanded(
            child: TextField(
              onChanged: controller.search,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(
                  vertical: 0,
                  horizontal: 10,
                ),
                filled: true,
                fillColor: Theme.of(context).backgroundColor,
                hintText: "Search Your Product",
                hintStyle: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 12,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(17),
                  borderSide: BorderSide.none,
                ),
                suffixIcon: Icon(
                  Icons.search,
                  size: 16,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
