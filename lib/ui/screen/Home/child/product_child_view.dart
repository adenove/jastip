import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/home_controller.dart';
import 'package:jastip/ui/screen/DetailProduct/detail_product_view.dart';
import 'package:jastip/ui/widget/other_widget/title_text.dart';
import 'package:jastip/ui/widget/product_widget/product_card_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class ProductView extends StatelessWidget {
  final HomeController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleText(name: "Products"),
          SizedBox(height: 20),
          _product(),
        ],
      ),
    );
  }

  Widget _product() {
    return Obx(() {
      if (controller.productLoadedEror) {
        return Center(
          child: TextCustom(
            title: "Mohon Perikas Konesksi anda",
            bold: true,
            color: Colors.grey[400],
          ),
        );
      }

      if (!controller.productLoaded) {
        if (controller.products.length < 1) {
          return Center(
            child: TextCustom(
              title: "Belum ada Product",
              bold: true,
              color: Colors.grey[400],
            ),
          );
        }
        return GridView.builder(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: .8,
          ),
          physics: NeverScrollableScrollPhysics(),
          itemCount: controller.products.length,
          itemBuilder: (_, i) {
            return ProductCard(
              tap: () {
                Get.to(
                  () => DetailProductView(),
                  arguments: controller.products[i],
                );
              },
              product: controller.products[i],
            );
          },
        );
      }

      return WaitingIndicator();
    });
  }
}
