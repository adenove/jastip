import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/home_controller.dart';
import 'package:jastip/ui/widget/other_widget/title_text.dart';
import 'package:jastip/ui/widget/button_widget/button_list_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class CategoryView extends StatelessWidget {
  final HomeController controller = Get.find();
  final List<Color> color = [
    Color(0xffF693B8),
    Color(0xffF6DF93),
    Color(0xff83F8A2),
    Color(0xff66E1F5),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleText(name: "Categories"),
          SizedBox(height: 20),
          _categoryList(),
        ],
      ),
    );
  }

  Widget _categoryList() {
    return Obx(() {
      if (controller.categoryLoadedEror) {
        return Center(
          child: TextCustom(
            title: "Mohon Perikas Konesksi anda",
            bold: true,
            color: Colors.grey[400],
          ),
        );
      }

      if (!controller.categoryLoaded) {
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: BouncingScrollPhysics(),
          child: Row(
            children: List.generate(
              controller.catrgories.length,
              (index) => ButtonList(
                tap: () {
                  controller.getProductByIdCategory(
                    controller.catrgories[index].id,
                  );
                },
                name: controller.catrgories[index].name,
                color: color[index],
              ),
            ),
          ),
        );
      }

      return WaitingIndicator();
    });
  }
}
