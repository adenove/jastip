import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/home_controller.dart';
import 'package:jastip/ui/widget/other_widget/sidebar_widget.dart';
import 'child/category_child_view.dart';
import 'child/header_child_view..dart';
import 'child/product_child_view.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HomeController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            HeaderView(),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                child: RefreshIndicator(
                  onRefresh: controller.dataMain,
                  child: ListView(
                    children: [
                      CategoryView(),
                      ProductView(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      drawer: Drawer(
        child: Sidebar(),
      ),
    );
  }
}
