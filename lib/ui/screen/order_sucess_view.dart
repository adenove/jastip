import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/ui/screen/ChatPage/chat_detail_view/chat_detail_view.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';

class OrderSucessView extends StatelessWidget {
  final UserModel user;

  OrderSucessView({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(tap: Get.back),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    AssetsImg.checklist,
                    width: 151,
                    height: 151,
                    fit: BoxFit.contain,
                  ),
                  SizedBox(height: 40),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Text(
                      "Order anda sedang diproses,mohon menunggu konfirmasi penjual",
                      style: TextStyle(
                        fontSize: 18,
                        letterSpacing: 2,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "*",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            "Chat Penjual Untuk Melakukan Transaksi Selanjutnya",
                            style: TextStyle(
                              fontSize: 18,
                              letterSpacing: 2,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 40),
                  SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 60,
                        vertical: 20,
                      ),
                      child: ButtonPrimary(
                        tap: () {
                          Get.to(() => ChatDetailView(user: user));
                        },
                        title: "Chat",
                        // bold: true,
                        textSize: 22,
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
