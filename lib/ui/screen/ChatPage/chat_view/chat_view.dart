import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/chat_controller.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';
import 'child/chat_list_view.dart';

class Chat extends GetView<ChatController> {
  Chat({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ChatController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(
              title: "Chat",
              tap: Get.back,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: GetX<ChatController>(
                  builder: (c) {
                    if (c.loadEror.value) {
                      return TextCustom(title: "Terjadi Kesalahan", bold: true);
                    }

                    if (!c.loadData.value) {
                      return ListView.builder(
                        itemCount: c.userList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ChatList(chat: c.userList[index]);
                        },
                      );
                    }

                    return Center(child: WaitingIndicator());
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
