import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/chat_user_list_model.dart';
import 'package:jastip/core/controller/chat_controller.dart';
import 'package:jastip/ui/screen/ChatPage/chat_detail_view/chat_detail_view.dart';
import 'package:jastip/ui/widget/image_widget/image_radius_widget.dart';

class ChatList extends StatelessWidget {
  final ChatUserListModel chat;
  final ChatController controller = Get.find();

  ChatList({this.chat});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(
          () => ChatDetailView(
            user: chat.dataUser,
          ),
        );
      },
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: Theme.of(context).accentColor,
          ),
          borderRadius: BorderRadius.circular(20),
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 5,
        ),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: ImageRadius(
                img: chat.dataUser.img,
                width: 60,
                height: 60,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 10),
                      child: Text(
                        chat.dataUser.name == null
                            ? chat.dataUser.email
                            : chat.dataUser.name,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(height: 5),
                    Container(
                      padding: EdgeInsets.only(bottom: 20),
                      child: Text(
                        "${chat.lastMessage.message}",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            notif(context),
          ],
        ),
      ),
    );
  }

  Widget notif(BuildContext context) {
    if (chat.unread["${chat.user}"] != null) {
      if (chat.unread["${chat.user}"] > 0) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
          decoration: BoxDecoration(
            color: Theme.of(context).accentColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Text(
            "${chat.unread["${chat.user}"]}",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
      }
    }

    return SizedBox();
  }
}
