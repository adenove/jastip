import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/chat_detail_controller.dart';

class Send extends StatelessWidget {
  final ChatDetailController controller = Get.find();
  final message = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: TextField(
            onTap: () {
              print("a");
              controller.checkMessage();
            },
            minLines: 1,
            maxLines: 5,
            controller: message,
            decoration: InputDecoration(
                hintText: "Masukan Pesan",
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,
                    color: Theme.of(context).accentColor,
                  ),
                )),
          ),
        ),
        SizedBox(width: 10),
        GestureDetector(
          onTap: () {
            if (message.text.isNotEmpty) {
              controller.sendMessage(message.text);
              message.clear();
            }
          },
          child: Icon(
            Icons.send,
            color: Theme.of(context).accentColor,
          ),
        ),
      ],
    );
  }
}
