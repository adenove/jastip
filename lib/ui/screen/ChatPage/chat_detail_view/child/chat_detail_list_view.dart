import 'package:flutter/material.dart';
import 'package:jastip/Model/chat_model.dart';
import 'package:jastip/core/services/auth_service.dart';

class ChatDetailList extends StatelessWidget {
  final ChatModel chat;

  ChatDetailList({this.chat});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        mainAxisAlignment: chat.senderId == AuthService.auth.currentUser.uid
            ? MainAxisAlignment.end
            : MainAxisAlignment.start,
        children: [
          Flexible(
            child: Container(
              padding: EdgeInsets.symmetric(
                vertical: 10,
                horizontal: 20,
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(
                  width: 1,
                  color: Theme.of(context).accentColor,
                ),
              ),
              child: Text(chat.message),
            ),
          ),
        ],
      ),
    );
  }
}
