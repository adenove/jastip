import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/controller/chat_detail_controller.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

import 'child/chat_detail_list_view.dart';
import 'child/send_view.dart';

class ChatDetailView extends GetView<ChatDetailController> {
  final UserModel user;
  ChatDetailView({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ChatDetailController(uid: user.id));
    return Scaffold(
      body: SafeArea(
        child: Obx(() {
          if (controller.loadIdEror.value) {
            return TextCustom(
              title: "Terjadi Kesalahan",
              bold: true,
            );
          }

          if (!controller.loadIdChat.value) {
            return Column(
              children: [
                AppBarData(
                  user: user,
                  chat: true,
                  tap: Get.back,
                ),
                Expanded(
                  child: GetX<ChatDetailController>(builder: (c) {
                    if (c.loadErorChat.value) {
                      return Center(
                        child: TextCustom(
                          title: "Terjadi Kesalahan",
                        ),
                      );
                    }

                    if (!c.loadChat.value) {
                      return Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: ListView.builder(
                          reverse: true,
                          itemCount: c.chats.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ChatDetailList(
                              chat: c.chats[index],
                            );
                          },
                        ),
                      );
                    }

                    return WaitingIndicator();
                  }),
                ),
                Container(
                  padding: EdgeInsets.all(20),
                  child: Send(),
                ),
              ],
            );
          }

          return Center(child: WaitingIndicator());
        }),
      ),
    );
  }
}
