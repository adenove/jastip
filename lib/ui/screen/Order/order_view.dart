import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/core/controller/order_page_controller.dart';
import 'package:jastip/ui/widget/order_widget/order_list_widget.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class OrderView extends GetView<OrderPageController> {
  const OrderView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(OrderPageController());
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(title: "My Order", tap: Get.back),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Obx(() {
                  if (controller.loadEror.value) {
                    return ListView(
                      children: [
                        SizedBox(height: Get.height / 2.5),
                        Center(
                          child: TextCustom(
                            title: "Terjadi Kesalahan",
                            color: Colors.grey,
                            bold: true,
                          ),
                        ),
                      ],
                    );
                  }

                  if (!controller.loadData.value) {
                    if (controller.listOrder.length < 1) {
                      return ListView(
                        children: [
                          SizedBox(height: Get.height / 2.5),
                          Center(
                            child: TextCustom(
                              title: "Anda Belum Melakukan Order",
                              color: Colors.grey,
                              bold: true,
                            ),
                          ),
                        ],
                      );
                    }

                    return ListView.builder(
                      itemCount: controller.listOrder.length,
                      itemBuilder: (BuildContext context, int index) {
                        return OrderList(
                          tap: () {
                            if (controller.listOrder[index].status != -1 &&
                                controller.listOrder[index].status != 2) {
                              _orderAction(controller.listOrder[index]);
                            }
                          },
                          data: controller.listOrder[index],
                        );
                      },
                    );
                  }

                  return WaitingIndicator();
                }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _orderAction(OrderDataModel data) {
    Get.defaultDialog(
      title: "Anda Memesan Product",
      content: Obx(() {
        if (controller.statusRefresh.value) {
          return WaitingIndicator();
        }

        return Container(
          child: Column(
            children: [
              TextCustom(
                title: "Product",
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "${data.product.name}",
                bold: true,
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "Jumlah",
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "${data.count}",
                bold: true,
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  data.status == 0
                      ? ElevatedButton(
                          onPressed: () => controller.actionOrder(
                              data.id, data.product.id, -1),
                          child: Text('Batalkan'),
                          style: ElevatedButton.styleFrom(primary: Colors.red),
                        )
                      : ElevatedButton(
                          onPressed: () => controller.actionOrder(
                              data.id, data.product.id, 2),
                          child: Text('Selesaikan Transaksi'),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.green),
                        ),
                  SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: Get.back,
                    child: Text('Close'),
                    style: ElevatedButton.styleFrom(primary: Colors.grey),
                  ),
                ],
              )
            ],
          ),
        );
      }),
    );
  }
}
