import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/seller_controller.dart';
import 'package:jastip/ui/screen/Seller/Page/history_seller_view.dart';
import 'package:jastip/ui/screen/Seller/Page/order_seller_view.dart';
import 'package:jastip/ui/screen/Seller/Page/product_manager_view.dart';
import 'package:jastip/ui/screen/Seller/Page/feed_view.dart';
import 'package:jastip/ui/widget/other_widget/sidebar_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';
import 'Child/header_child_view.dart';

class SellerView extends GetView<SellerController> {
  final List<String> title = ["Feed", "Order", "History"];
  SellerView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: GetBuilder<SellerController>(
          builder: (c) {
            return Column(
              children: [
                HeaderView(title: title[c.menuIndex]),
                Expanded(
                  child: GetBuilder<SellerController>(
                    builder: (c) {
                      return IndexedStack(
                        index: c.menuIndex,
                        children: [
                          FeedView(),
                          OrderSellerView(),
                          HistorySellerView(),
                        ],
                      );
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                  ),
                  child: Row(
                    children: [
                      menu("Feed", 0, false, () => controller.setMenuIndex(0)),
                      GetX<SellerController>(builder: (c) {
                        return menu("Order", c.order.value, true,
                            () => c.setMenuIndex(1));
                      }),
                      menu("Histroy", 0, false,
                          () => controller.setMenuIndex(2)),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(bottom: 60, left: 20),
        child: FloatingActionButton(
          backgroundColor: Theme.of(context).accentColor,
          child: Icon(Icons.add),
          onPressed: () {
            Get.to(() => ProductManagerView());
          },
        ),
      ),
      drawer: Drawer(
        child: Sidebar(seller: true),
      ),
    );
  }

  Widget menu(String title, int notif, bool center, Function tap) {
    return Expanded(
      child: GestureDetector(
        onTap: tap,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(
                width: center ? 1 : 0,
                color: Colors.white,
              ),
              right: BorderSide(
                width: center ? 1 : 0,
                color: Colors.white,
              ),
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Flexible(
                child: Text(
                  "$title".toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(width: 5),
              if (notif > 0)
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: TextCustom(
                    title: "$notif",
                    textSize: 12,
                    bold: true,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
