import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/history_seller_controller.dart';
import 'package:jastip/ui/widget/order_widget/order_listcard_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class HistorySellerView extends GetView<HistorySellerController> {
  const HistorySellerView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(HistorySellerController());
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Obx(() {
        if (controller.historyLoadEror.value) {
          return ListView(
            children: [
              SizedBox(height: Get.height / 2.2),
              Center(
                child: TextCustom(
                  title: "terjadi kesalahan",
                  bold: true,
                  color: Colors.grey,
                ),
              ),
            ],
          );
        }

        if (!controller.historyLoad.value) {
          if (controller.historys.length < 1) {
            return ListView(
              children: [
                SizedBox(height: Get.height / 2.2),
                Center(
                  child: TextCustom(
                    title: "History Masih kosong",
                    bold: true,
                    color: Colors.grey,
                  ),
                ),
              ],
            );
          }

          return ListView.builder(
            itemCount: controller.historys.length,
            itemBuilder: (BuildContext context, int index) {
              return OrderListCardWidget(
                tap: () {},
                order: controller.historys[index],
              );
            },
          );
        }

        return Center(child: WaitingIndicator());
      }),
    );
  }
}
