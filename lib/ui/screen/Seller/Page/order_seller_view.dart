import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/core/controller/order_seller_controller.dart';
import 'package:jastip/ui/widget/order_widget/order_listcard_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class OrderSellerView extends GetView<OrderSellerController> {
  const OrderSellerView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(OrderSellerController());
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: GetX<OrderSellerController>(builder: (c) {
            if (c.orderEror.value) {
              return ListView(
                children: [
                  SizedBox(height: Get.height / 2.2),
                  TextCustom(
                    title: "terjadi kesalahan",
                    bold: true,
                    color: Colors.grey,
                  ),
                ],
              );
            }

            if (!c.orderRefresh.value) {
              if (c.orders.length < 1) {
                return ListView(
                  children: [
                    SizedBox(height: Get.height / 2.2),
                    Center(
                      child: TextCustom(
                        title: "Order Masih kosong",
                        bold: true,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                );
              }
              return ListView.builder(
                itemCount: c.orders.length,
                itemBuilder: (BuildContext context, int index) {
                  return Obx(
                    () => OrderListCardWidget(
                      longPress: () {
                        controller.longPress(c.orders[index].id);
                      },
                      selected: controller.selects.contains(
                        c.orders[index].id,
                      ),
                      tap: () {
                        if (c.orders[index].status == 0) {
                          _orderAction(c.orders[index]);
                        }
                        // if (c.selects.length > 0) {
                        //   if (c.selects.contains(c.orders[index].id)) {
                        //     controller.selects.remove(c.orders[index].id);
                        //   } else {
                        //     controller.selects.add(c.orders[index].id);
                        //   }
                        //   return;
                        // }
                      },
                      order: c.orders[index],
                      // widgetStatus: false,
                    ),
                  );
                },
              );
            }

            return Center(child: WaitingIndicator());
          }),
        ),
      ),
    );
  }

  void _orderAction(OrderDataModel data) {
    String name = data.userOrder.name != null
        ? data.userOrder.name
        : data.userOrder.email;

    Get.defaultDialog(
      title: "$name",
      content: Obx(() {
        if (controller.statusRefresh.value) {
          return WaitingIndicator();
        }
        return Container(
          child: Column(
            children: [
              TextCustom(
                title: "Memesan Product",
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "${data.product.name}",
                bold: true,
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "Jumlah",
              ),
              SizedBox(height: 10),
              TextCustom(
                title: "${data.count}",
                bold: true,
              ),
              SizedBox(height: 20),
              Row(
                children: [
                  data.status == 1
                      ? ElevatedButton(
                          onPressed: () => controller.actionOrder(
                              data.id, data.product.id, 2),
                          child: Text('Order Selesai'),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.green),
                        )
                      : ElevatedButton(
                          onPressed: () => controller.actionOrder(
                              data.id, data.product.id, 1),
                          child: Text('Confirm'),
                          style:
                              ElevatedButton.styleFrom(primary: Colors.green),
                        ),
                  SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: () =>
                        controller.actionOrder(data.id, data.product.id, -1),
                    child: Text('Batalkan'),
                    style: ElevatedButton.styleFrom(primary: Colors.red),
                  ),
                  SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: Get.back,
                    child: Text('Close'),
                    style: ElevatedButton.styleFrom(primary: Colors.grey),
                  ),
                ],
              )
            ],
          ),
        );
      }),
    );
  }
}
