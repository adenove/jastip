import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/category_model.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/controller/product_manager_controller.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/util/validator.dart';
import 'package:jastip/ui/screen/Seller/seller_view.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_square_picker_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';

class ProductManagerView extends GetView<ProductManagerController> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final String title;
  final String idProduct;
  final bool fromBuyer;
  final bool admin;
  final idCode = TextEditingController();
  final name = TextEditingController();
  final price = TextEditingController();
  final stock = TextEditingController();
  final size = TextEditingController();
  final color = TextEditingController();
  final location = TextEditingController();

  ProductManagerView({
    this.title = "Tambah Product",
    this.idProduct,
    this.fromBuyer = false,
    this.admin = false,
  });

  void submit() {
    if (_formKey.currentState.validate()) {
      controller.actionProduct(
        ProductModel(
          id: idProduct,
          idUser: AuthService.auth.currentUser.uid,
          idCode: idCode.text,
          idKategory: controller.product.value.idKategory,
          name: name.text,
          price: int.parse(price.text),
          stock: int.parse(stock.text),
          img: controller.product.value.img,
          size: size.text,
          color: color.text,
          location: location.text,
          createdAt: Timestamp.fromDate(
            DateTime.now(),
          ),
        ),
      );
    }
  }

  Future<bool> _willPopCallback() async {
    if (fromBuyer) {
      Get.off(() => SellerView());
    } else {
      Get.back();
    }
    return true; // return true if the route to be popped
  }

  @override
  Widget build(BuildContext context) {
    Get.put(ProductManagerController(id: this.idProduct, fromBuyer: fromBuyer));
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        body: SafeArea(
          child: Container(
            child: Column(
              children: [
                AppBarData(title: title, tap: _willPopCallback),
                Expanded(
                  child: ListView(
                    children: [
                      GetBuilder<ProductManagerController>(builder: (c) {
                        return ImageSquare(
                          tap: () {
                            if (!admin) {
                              c.changeProfil();
                            }
                          },
                          picker: !admin,
                          file: c.file,
                          img: controller.product.value.img,
                        );
                      }),
                      Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        child: Obx(() {
                          if (controller.load) {
                            return WaitingIndicator();
                          }

                          return Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                TextFormField(
                                  enabled: false,
                                  validator: Validator.isNotEmpty,
                                  controller: idCode
                                    ..text = controller.product.value.idCode,
                                  decoration: InputDecoration(
                                    labelText: "ID",
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  enabled: !admin,
                                  validator: Validator.isNotEmpty,
                                  controller: name
                                    ..text = controller.product.value.name,
                                  decoration: InputDecoration(
                                    labelText: "Nama Product",
                                  ),
                                ),
                                SizedBox(height: 10),
                                controller.wait.value
                                    ? WaitingIndicator()
                                    : DropdownButton(
                                        value:
                                            controller.product.value.idKategory,
                                        hint: Text("Category"),
                                        isExpanded: true,
                                        items: controller.categories
                                            .map((CategoryModel c) {
                                          return DropdownMenuItem<String>(
                                            value: c.id,
                                            child: Text(c.name),
                                          );
                                        }).toList(),
                                        onChanged: !admin
                                            ? (_) {
                                                controller
                                                    .setCategoryProduct(_);
                                              }
                                            : null,
                                      ),
                                TextFormField(
                                  validator: Validator.onlyNumber,
                                  enabled: !admin,
                                  controller: price
                                    ..text = controller.product.value.price
                                        .toString(),
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    labelText: "Price",
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  enabled: !admin,
                                  validator: Validator.onlyNumber,
                                  controller: stock
                                    ..text = controller.product.value.stock
                                        .toString(),
                                  keyboardType: TextInputType.number,
                                  decoration: InputDecoration(
                                    labelText: "Stock",
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  enabled: !admin,
                                  validator: Validator.isNotEmpty,
                                  controller: size
                                    ..text = controller.product.value.size,
                                  decoration: InputDecoration(
                                    labelText: "Size",
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  enabled: !admin,
                                  validator: Validator.isNotEmpty,
                                  controller: color
                                    ..text = controller.product.value.color,
                                  decoration: InputDecoration(
                                    labelText: "Color",
                                  ),
                                ),
                                SizedBox(height: 10),
                                TextFormField(
                                  enabled: !admin,
                                  validator: Validator.isNotEmpty,
                                  controller: location
                                    ..text = controller.product.value.location,
                                  decoration: InputDecoration(
                                    labelText: "Location",
                                  ),
                                ),
                                SizedBox(height: 40),
                                !admin
                                    ? controller.wait.value
                                        ? WaitingIndicator()
                                        : ButtonPrimary(
                                            tap: submit,
                                            padding: EdgeInsets.symmetric(
                                              vertical: 10,
                                              horizontal: 30,
                                            ),
                                            title: idProduct == null
                                                ? "Tambah Product"
                                                : "Edit Product",
                                          )
                                    : SizedBox(),
                              ],
                            ),
                          );
                        }),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
