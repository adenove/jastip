import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/core/controller/feed_controller.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/product_widget/product_list_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class FeedView extends GetView<FeedController> {
  @override
  Widget build(BuildContext context) {
    Get.put(FeedController());
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Obx(() {
        if (controller.feedEror) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: [
                  TextCustom(title: "Terjadi Ksalahan"),
                  SizedBox(height: 20),
                  ButtonPrimary(
                    tap: controller.getFeed,
                    title: "Refresh",
                    padding: EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 10,
                    ),
                  ),
                ],
              ),
            ],
          );
        }

        if (controller.feedWait) {
          return WaitingIndicator();
        }

        if (controller.feed.length < 1) {
          return Center(
            child: TextCustom(
              title: "Product Masih Kosong",
              bold: true,
              color: Colors.grey[400],
            ),
          );
        }

        return ListView.builder(
          itemCount: controller.feed.length,
          itemBuilder: (_, i) {
            return ProductListCard(
              edit: controller.edit,
              data: controller.feed[i],
              delete: controller.delete,
              isYou: true,
            );
          },
        );
      }),
    );
  }
}
