import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/controller/profil_toko_controller.dart';
import 'package:jastip/ui/screen/OrderProduct/order_product_view.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/other_widget/user_profil_widget.dart';
import 'package:jastip/ui/widget/other_widget/waiting_indicator_widget.dart';
import 'package:jastip/ui/widget/product_widget/product_list_widget.dart';
import 'package:jastip/ui/widget/text_widget/text_custom_widget.dart';

class ProfilTokoView extends GetView<ProfilTokoController> {
  final UserModel user;
  ProfilTokoView({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(ProfilTokoController(uid: user.id));
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(tap: Get.back),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: RefreshIndicator(
                  onRefresh: () async {
                    controller.getProducts(user.id);
                    return true;
                  },
                  child: ListView(
                    children: [
                      UserProfil(user: user),
                      Obx(() {
                        if (controller.productEror) {
                          return TextCustom(
                            title: "Prikas Koneksi Anda",
                          );
                        }

                        if (!controller.productLoad) {
                          return Container(
                            margin: EdgeInsets.only(top: 40),
                            child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: controller.products.length,
                              itemBuilder: (_, i) {
                                return ProductListCard(
                                  buy: () => Get.to(() => OrderProductView(
                                        data: controller.products[i],
                                      )),
                                  data: controller.products[i],
                                );
                              },
                            ),
                          );
                        }

                        return Column(
                          children: [
                            SizedBox(height: 100),
                            WaitingIndicator(),
                          ],
                        );
                      }),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
