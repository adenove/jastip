import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/config/assets_config.dart';
import 'package:jastip/core/binding/auth_binding.dart';
import 'package:jastip/ui/screen/Auth/login_view.dart';
import 'package:jastip/ui/screen/Auth/register_view.dart';
import 'package:jastip/ui/widget/button_widget/button_outlined_widget.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';

class SplashView extends StatelessWidget {
  const SplashView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: ListView(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            children: [
              Image.asset(
                AssetsImg.jastip,
                width: 150,
                height: 150,
              ),
              SizedBox(height: 80),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ButtonPrimary(
                      tap: () => Get.to(
                        () => LoginView(),
                        binding: AuthBinding(),
                      ),
                      title: "Login",
                    ),
                    SizedBox(height: 20),
                    ButtonOutlined(
                      tap: () => Get.to(
                        () => RegisterView(),
                        binding: AuthBinding(),
                      ),
                      title: "Register",
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
