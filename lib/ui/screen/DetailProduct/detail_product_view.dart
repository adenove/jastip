import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/config/helper.dart';
import 'package:jastip/core/binding/seller_binding.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/DetailProduct/controller/detail_product_controller.dart';
import 'package:jastip/ui/screen/OrderProduct/order_product_view.dart';
import 'package:jastip/ui/screen/Seller/Page/product_manager_view.dart';
import 'package:jastip/ui/widget/other_widget/app_bar_data_widget.dart';
import 'package:jastip/ui/widget/button_widget/button_primary_widget.dart';
import 'package:jastip/ui/widget/image_widget/image_square_picker_widget.dart';

class DetailProductView extends GetView<DetailProductController> {
  const DetailProductView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Get.put(DetailProductController());

    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            AppBarData(
              title: "Shipping",
              tap: Get.back,
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
                child: Obx(
                  () {
                    if (controller.load.value)
                      return Center(
                        child: CircularProgressIndicator(),
                      );

                    return ListView(
                      children: [
                        ImageSquare(img: controller.data.img, picker: false),
                        SizedBox(height: 40),
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 20),
                          child: Column(
                            children: [
                              listDetailProduct(
                                title: "Product",
                                value: controller.data.name,
                              ),
                              listDetailProduct(
                                title: "Size",
                                value: controller.data.size,
                              ),
                              listDetailProduct(
                                title: "Color",
                                value: controller.data.color,
                              ),
                              listDetailProduct(
                                title: "Price",
                                value: Helper.rupiah(controller.data.price),
                              ),
                              listDetailProduct(
                                title: "Location",
                                value: controller.data.location,
                              ),
                              listDetailProduct(
                                title: "Stock",
                                value: controller.data.stock.toString(),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20),
                        AuthService.auth.currentUser.uid !=
                                controller.data.idUser
                            ? ButtonPrimary(
                                tap: () => Get.to(() =>
                                    OrderProductView(data: controller.data)),
                                title: "Order",
                                bold: true,
                                textColor: Colors.white,
                                padding: EdgeInsets.symmetric(vertical: 15),
                              )
                            : ButtonPrimary(
                                tap: () => changeToSeller(context),
                                title: "Edit Product",
                                bold: true,
                                textColor: Colors.white,
                                padding: EdgeInsets.symmetric(vertical: 15),
                              ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void changeToSeller(BuildContext _) {
    Get.defaultDialog(
      title: "Info",
      content: Padding(
        padding: const EdgeInsets.all(20),
        child: Text(
          "Switch to Seller Untuk Mengedit Product ini?",
          textAlign: TextAlign.center,
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () {
            Get.offAll(
              () => ProductManagerView(
                idProduct: controller.data.id,
                fromBuyer: true,
              ),
              binding: SellerBinding(),
            );
          },
          child: Text("ya"),
          style: ElevatedButton.styleFrom(
            primary: Theme.of(_).accentColor,
          ),
        ),
        ElevatedButton(
          onPressed: () => Get.back(),
          child: Text("Batal"),
          style: ElevatedButton.styleFrom(
            primary: Colors.grey,
          ),
        ),
      ],
    );
  }

  Widget listDetailProduct({String title = "", String value = ""}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "$title",
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          SizedBox(width: 10, child: Center(child: Text(":"))),
          Expanded(
              child: Text(
            "$value",
            style: TextStyle(
              fontSize: 18,
            ),
          )),
        ],
      ),
    );
  }
}
