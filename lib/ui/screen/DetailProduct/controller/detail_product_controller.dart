import 'package:get/get.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/firestore/product_services.dart';

class DetailProductController extends GetxController {
  final streamProductData = ProductModel().obs;
  ProductModel get data => streamProductData.value;

  final load = false.obs;

  @override
  void onInit() {
    super.onInit();
    streamProduct();
  }

  void streamProduct() {
    if (Get.arguments == null) return;
    streamProductData(Get.arguments);
    load(true);
    streamProductData.bindStream(
      ProductServices.streamProductbyId(Get.arguments.id).asyncMap((event) {
        load(false);
        return event;
      }),
    );
  }
}
