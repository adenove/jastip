class AssetsImg {
  static String assetsBase = "assets/";
  static String logoFolder = assetsBase + "logo/";
  static String other = assetsBase + "other/";
  static String jastip = logoFolder + "jastip.png";
  static String checklist = logoFolder + "checklist.png";
  static String noProfil = other + "no-profil.png";
  static String noImg = other + "no-img.png";
}
