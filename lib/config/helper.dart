import 'dart:math';

class Helper {
  static String rupiah(value, {String separator = '.', String trailing = ''}) {
    return "Rp. " +
        value.toString().replaceAllMapped(
            new RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))'),
            (Match m) => '${m[1]}$separator') +
        trailing;
  }

  static String getRandString(int len) {
    const _chars =
        '123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();

    return String.fromCharCodes(
      Iterable.generate(
        len,
        (_) => _chars.toUpperCase().codeUnitAt(
              _rnd.nextInt(_chars.length),
            ),
      ),
    );
  }
}
