// To parse this JSON data, do
//
//     final productModel = productModelFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

List<ProductModel> productModelFromJson(String str) => List<ProductModel>.from(
    json.decode(str).map((x) => ProductModel.fromJson(x)));

String productModelToJson(List<ProductModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductModel {
  ProductModel({
    this.id,
    this.idUser,
    this.idCode,
    this.idKategory,
    this.name,
    this.price = 0,
    this.stock = 0,
    this.img,
    this.size,
    this.color,
    this.location,
    this.createdAt,
  });

  String id;
  String idUser;
  String idCode;
  String idKategory;
  String name;
  int price;
  int stock;
  String img;
  String size;
  String color;
  String location;
  Timestamp createdAt;

  factory ProductModel.fromJson(Map<String, dynamic> json) => ProductModel(
        id: json["id"],
        idUser: json["id_user"],
        idCode: json["id_code"],
        idKategory: json["id_kategory"],
        name: json["name"],
        stock: json["stock"],
        price: json["price"],
        img: json["img"] == null ? null : json["img"],
        size: json["size"] == null ? null : json["size"],
        color: json["color"] == null ? null : json["color"],
        location: json["location"] == null ? null : json["location"],
        createdAt: json["created_at"],
      );

  factory ProductModel.snapshot(DocumentSnapshot json) => ProductModel(
        id: json.id,
        idUser: json["id_user"],
        idCode: json["id_code"],
        idKategory: json["id_kategory"],
        name: json["name"],
        stock: json["stock"],
        price: json["price"],
        img: json["img"] == null ? null : json["img"],
        size: json["size"] == null ? null : json["size"],
        color: json["color"] == null ? null : json["color"],
        location: json["location"] == null ? null : json["location"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_user": idUser,
        "id_code": idCode,
        "id_kategory": idKategory,
        "name": name,
        "price": price,
        "stock": stock,
        "img": img == null ? null : img,
        "size": size == null ? null : size,
        "color": color == null ? null : color,
        "location": location == null ? null : location,
        "created_at": createdAt,
      };
}
