// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

import 'package:jastip/Model/product_model.dart';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.id,
    this.name,
    this.address,
    this.img,
    this.product,
    this.email,
    this.phone,
  });

  String id;
  String name;
  String address;
  String img;
  List<ProductModel> product;
  String email;
  String phone;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        id: json["id"],
        name: json["name"],
        address: json["address"],
        img: json["img"],
        product: json["product"] == null ? [] : json["product"],
        email: json["email"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "address": address,
        "img": img,
        "product": product,
        "email": email,
        "phone": phone,
      };
}
