// To parse this JSON data, do
//
//     final orderModel = orderModelFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

List<OrderModel> orderModelFromJson(String str) =>
    List<OrderModel>.from(json.decode(str).map((x) => OrderModel.fromJson(x)));

String orderModelToJson(List<OrderModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderModel {
  OrderModel({
    this.id,
    this.idProduct,
    this.idUserProduct,
    this.idUserOrder,
    this.count,
    this.status,
    this.createdAt,
  });

  String id;
  String idProduct;
  String idUserProduct;
  String idUserOrder;
  int count;
  int status;
  Timestamp createdAt;

  factory OrderModel.fromJson(Map<String, dynamic> json) => OrderModel(
        id: json["id"] == null ? null : json["id"],
        idProduct: json["id_product"],
        idUserProduct: json["id_user_product"],
        idUserOrder: json["id_user_order"],
        count: json["count"],
        status: json["status"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "id_product": idProduct,
        "id_user_product": idUserProduct,
        "id_user_order": idUserOrder,
        "count": count,
        "status": status,
        "created_at": createdAt,
      };
}
