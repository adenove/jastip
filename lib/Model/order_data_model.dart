// To parse this JSON data, do
//
//     final orderDataModel = orderDataModelFromJson(jsonString);

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/Model/user_model.dart';

List<OrderDataModel> orderDataModelFromJson(String str) =>
    List<OrderDataModel>.from(
        json.decode(str).map((x) => OrderDataModel.fromJson(x)));

String orderDataModelToJson(List<OrderDataModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class OrderDataModel {
  OrderDataModel({
    this.id,
    this.userOrder,
    this.userProduct,
    this.product,
    this.status,
    this.count,
    this.createdAt,
  });

  String id;
  UserModel userOrder;
  UserModel userProduct;
  ProductModel product;
  int status;
  int count;
  Timestamp createdAt;

  factory OrderDataModel.fromJson(Map<String, dynamic> json) => OrderDataModel(
        id: json["id"],
        userOrder: json["user_order"],
        userProduct: json["user_product"],
        product: json["product"],
        status: json["status"],
        count: json["count"],
        createdAt: json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_product": userProduct,
        "user_order": userOrder,
        "product": product,
        "status": status,
        "count": count,
        "created_at": createdAt,
      };
}
