// To parse this JSON data, do
//
//     final chatUserListModel = chatUserListModelFromJson(jsonString);

import 'dart:convert';

import 'package:jastip/Model/chat_model.dart';
import 'package:jastip/Model/user_model.dart';

List<ChatUserListModel> chatUserListModelFromJson(String str) =>
    List<ChatUserListModel>.from(
        json.decode(str).map((x) => ChatUserListModel.fromJson(x)));

String chatUserListModelToJson(List<ChatUserListModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ChatUserListModel {
  ChatUserListModel({
    this.id,
    this.user,
    this.dataUser,
    this.lastMessage,
    this.unread,
  });

  String id;
  String user;
  UserModel dataUser;
  ChatModel lastMessage;
  Map<String, dynamic> unread;

  factory ChatUserListModel.fromJson(Map<String, dynamic> json) =>
      ChatUserListModel(
        id: json["id"],
        user: json["user"],
        dataUser: json["data_user"],
        lastMessage: json["last_message"],
        unread: json["unread"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user": user,
        "data_user": dataUser,
        "last_message": lastMessage,
        "unread": unread,
      };
}
