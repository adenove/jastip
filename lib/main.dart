import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/ui/screen/Splash/splash_view.dart';
import 'package:jastip/ui/screen/root_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Jastip',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        accentColor: Color(0xffF8C978),
        backgroundColor: Color(0xffF9F8F4),
        highlightColor: Color(0xff95E6E7),
      ),
      home: AuthService.auth.currentUser == null ? SplashView() : RootView(),
    );
  }
}
