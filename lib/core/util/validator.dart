import 'package:get/get.dart';

class Validator {
  static String isNotEmpty(String value) {
    if (value.isEmpty) {
      return "Harus Diisi";
    }
    return null;
  }

  static String onlyNumber(String value) {
    if (!GetUtils.isNumericOnly(value)) {
      return "Harus di isi angka";
    }
    return null;
  }
}
