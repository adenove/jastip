import 'package:image_picker/image_picker.dart';

class Util {
  static Future<String> getImageFromGallery() async {
    var picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    return pickedFile.path;
  }
}
