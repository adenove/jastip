import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jastip/core/services/firestore/collection.dart';

class PembayaranServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static CollectionReference pembayaran =
      firestore.collection(Collection.pembayaran);

  static Future<bool> checkPembayaran() async {
    try {
      var result = await pembayaran.doc("check").get();
      return result["terbayar"];
    } on FirebaseException catch (e) {
      throw e;
    }
  }
}
