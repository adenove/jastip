import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/collection.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class ProductServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  static CollectionReference product = firestore.collection(
    Collection.product,
  );

  static Future actionProduct(ProductModel newProduct, File img) async {
    try {
      String photoUrl;
      String idProduct;
      if (newProduct.id == null) {
        var result = await product.add(newProduct.toJson());
        idProduct = result.id;
      } else {
        await product.doc(newProduct.id).update(newProduct.toJson());
        idProduct = newProduct.id;
      }

      if (img != null) {
        var ref = firebaseStorage.ref(
          Collection.uploadProduct(
            AuthService.auth.currentUser.uid,
            idProduct,
          ),
        );
        await ref.putFile(img);
        photoUrl = await ref.getDownloadURL();
      }

      if (photoUrl != null) {
        await product.doc(idProduct).update({
          "img": photoUrl,
        });
      }
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<ProductModel>> getProductbyUid(String uid) async {
    try {
      var result = await product
          // .orderBy("created_at")
          .where("id_user", isEqualTo: uid)
          .get();
      List<ProductModel> data = [];
      result.docs.forEach((s) {
        ProductModel result = ProductModel.snapshot(s);
        result.id = s.id;
        data.add(result);
      });

      data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<ProductModel>> streamProductbyUid(String uid) {
    try {
      return product.where("id_user", isEqualTo: uid).snapshots().map((val) {
        List<ProductModel> data = [];
        val.docs.forEach((s) {
          ProductModel result = ProductModel.snapshot(s);
          result.id = s.id;
          data.add(result);
        });

        data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

        return data;
      });

      // List<ProductModel> data = [];
      // result.docs.forEach((s) {
      //   ProductModel result = ProductModel.snapshot(s);
      //   result.id = s.id;
      //   data.add(result);
      // });

      // data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

      // return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<ProductModel>> streamAllProudct() {
    try {
      return product.snapshots().map((result) {
        List<ProductModel> data = [];

        result.docs.forEach((s) {
          ProductModel result = ProductModel.snapshot(s);
          result.id = s.id;
          data.add(result);
        });

        data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

        return data;
      });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<ProductModel> streamProductbyId(String id) {
    try {
      return product.doc(id).snapshots().map((val) {
        return ProductModel.snapshot(val);
      });
      // // result.data()["id"] = id;
      // var dt = result.data();
      // ProductModel data = ProductModel(
      //   id: id,
      //   idUser: dt["id_user"],
      //   idCode: dt["id_code"],
      //   idKategory: dt["id_kategory"],
      //   name: dt["name"],
      //   img: dt["img"],
      //   location: dt["location"],
      //   price: dt["price"],
      //   size: dt["size"],
      //   stock: dt["stock"],
      //   color: dt["color"],
      //   createdAt: dt["created_at"],
      // );
      // return ProductModel.snapshot(result);
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<ProductModel> getProductbyId(String id) async {
    try {
      var result = await product.doc(id).get();
      // // result.data()["id"] = id;
      // var dt = result.data();
      // ProductModel data = ProductModel(
      //   id: id,
      //   idUser: dt["id_user"],
      //   idCode: dt["id_code"],
      //   idKategory: dt["id_kategory"],
      //   name: dt["name"],
      //   img: dt["img"],
      //   location: dt["location"],
      //   price: dt["price"],
      //   size: dt["size"],
      //   stock: dt["stock"],
      //   color: dt["color"],
      //   createdAt: dt["created_at"],
      // );
      return ProductModel.snapshot(result);
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<ProductModel>> getProductbyIdCategory(
      String idCategory) async {
    try {
      var result =
          await product.where("id_kategory", isEqualTo: idCategory).get();
      List<ProductModel> data = [];
      result.docs.forEach((s) {
        ProductModel result = ProductModel.snapshot(s);
        result.id = s.id;
        data.add(result);
      });
      data.sort((a, b) => b.createdAt.compareTo(a.createdAt));
      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future deleteProduct(String id) async {
    try {
      await product.doc(id).delete();
      await OrderServices.order
          .where("id_product", isEqualTo: id)
          .get()
          .then((value) {
        value.docs.forEach((element) async {
          await OrderServices.order.doc(element.id).delete();
        });
      });
      var ref = firebaseStorage.ref(
        Collection.uploadProduct(
          AuthService.auth.currentUser.uid,
          id,
        ),
      );
      await ref.delete();
    } on FirebaseException catch (e) {
      throw e;
    }
  }
}
