class Collection {
  static String user = "user";
  static String category = "category";
  static String product = "product";
  static String order = "order";
  static String pembayaran = "pembayaran";
  static String chat = "chat";
  static String userFolder(String uid) => "$user/$uid";
  static String profilFolder(String uid) => "${userFolder(uid)}/profil.jpg";
  static String productFolder(String uid) => "${userFolder(uid)}/$product";
  static String uploadProduct(String uid, String idProduct) =>
      "${productFolder(uid)}/$idProduct.jpg";
}
