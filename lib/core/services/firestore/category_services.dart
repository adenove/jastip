import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jastip/Model/category_model.dart';
import 'package:jastip/core/services/firestore/collection.dart';

class CategoryServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static CollectionReference category =
      firestore.collection(Collection.category);

  static Future<List<CategoryModel>> getCategory() async {
    List<CategoryModel> categorys = [];
    var get = await category.get();
    get.docs.forEach((e) {
      var result = e.data();
      categorys.add(CategoryModel(
        id: e.id,
        name: result["name"],
      ));
    });
    return categorys;
  }
}
