import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/services/firestore/product_services.dart';
import 'package:jastip/core/services/firestore/user_services.dart';
import 'collection.dart';

class OrderServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static CollectionReference order = firestore.collection(Collection.order);
  static CollectionReference product = firestore.collection(Collection.product);

  static Future addOrder(OrderModel newOrder) async {
    try {
      var refProduct = product.doc(newOrder.idProduct);
      var getproduct = await refProduct.get();
      var result = ProductModel.fromJson(getproduct.data());
      int calculatedStock = result.stock - newOrder.count;
      if (calculatedStock > -1) {
        await order.add(newOrder.toJson());
        await refProduct.update({"stock": calculatedStock});
      } else {
        throw FirebaseException(
          plugin: "Stock",
          message:
              "Pembelian Melebihi Stock ,Stock Tersedia Hanya ${result.stock}",
        );
      }
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<OrderModel>> streamOrderByUid(String uid) {
    try {
      return order.where("id_user_order", isEqualTo: uid).snapshots().map((q) {
        List<OrderModel> data = [];
        q.docs.forEach((e) {
          var list = e.data();
          OrderModel ordr = OrderModel.fromJson(list);
          ordr.id = e.id;
          data.add(ordr);
        });

        data.sort((a, b) {
          return a.createdAt.compareTo(b.createdAt);
        });

        return data;
      });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<OrderDataModel>> getSellerOrderByUid(String uid) async {
    try {
      var get = await order
          .where("id_user_product", isEqualTo: uid)
          .where("status", isEqualTo: 0)
          .get();
      List<OrderDataModel> data = await queryRef(uid, get);

      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<OrderModel>> streamOrderRequestByUid(String uid) {
    try {
      return order
          .where("id_user_product", isEqualTo: uid)
          .where("status", whereIn: [0, 1])
          .snapshots()
          .map((q) {
            List<OrderModel> data = [];
            q.docs.forEach((e) {
              var list = e.data();
              OrderModel ordr = OrderModel.fromJson(list);
              ordr.id = e.id;
              data.add(ordr);
            });

            data.sort((a, b) {
              return a.createdAt.compareTo(b.createdAt);
            });

            return data;
          });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<OrderModel>> streamAllOrder() {
    try {
      return order.where("status", whereIn: [0, 1]).snapshots().map((q) {
            List<OrderModel> data = [];
            q.docs.forEach((e) {
              var list = e.data();
              OrderModel ordr = OrderModel.fromJson(list);
              ordr.id = e.id;
              data.add(ordr);
            });

            data.sort((a, b) {
              return a.createdAt.compareTo(b.createdAt);
            });

            return data;
          });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<OrderModel>> streamHistoryByUid(String uid) {
    try {
      return order
          .where("id_user_product", isEqualTo: uid)
          .where("status", whereIn: [-1, 2])
          .snapshots()
          .map((q) {
            List<OrderModel> data = [];
            q.docs.forEach((e) {
              var list = e.data();
              OrderModel ordr = OrderModel.fromJson(list);
              ordr.id = e.id;
              data.add(ordr);
            });

            data.sort((a, b) {
              return a.createdAt.compareTo(b.createdAt);
            });

            return data;
          });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<OrderModel>> streamAllHistory() {
    try {
      return order.where("status", whereIn: [-1, 2]).snapshots().map((q) {
            List<OrderModel> data = [];
            q.docs.forEach((e) {
              var list = e.data();
              OrderModel ordr = OrderModel.fromJson(list);
              ordr.id = e.id;
              data.add(ordr);
            });

            data.sort((a, b) {
              return a.createdAt.compareTo(b.createdAt);
            });

            return data;
          });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<OrderDataModel>> getHistoryOrderByUid(String uid) async {
    try {
      var get = await order
          .where("id_user_product", isEqualTo: uid)
          .where("status", whereIn: [-1, 2]).get();
      List<OrderDataModel> data = await queryRef(uid, get);

      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<OrderModel> getOrderByid(String id) async {
    try {
      var get = await order.doc(id).get();
      return OrderModel.fromJson(get.data());
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<OrderDataModel>> queryRef(
    String uid,
    QuerySnapshot get,
  ) async {
    try {
      List<OrderDataModel> data = [];

      await Future.forEach(get.docs, (o) async {
        var result = o.data();
        var userOrder =
            await UserServices.user.doc(result["id_user_order"]).get();
        var userProduct =
            await UserServices.user.doc(result["id_user_product"]).get();
        var product =
            await ProductServices.product.doc(result["id_product"]).get();
        data.add(OrderDataModel(
          id: o.id,
          userOrder: UserModel.fromJson(userOrder.data()),
          userProduct: UserModel.fromJson(userProduct.data()),
          product: ProductModel.fromJson(product.data()),
          status: result["status"],
          count: result["count"],
          createdAt: result["created_at"],
        ));
      });

      data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<List<OrderDataModel>> joinTable(
    List<OrderModel> orderList,
  ) async {
    try {
      List<OrderDataModel> data = [];

      await Future.forEach(orderList, (OrderModel o) async {
        var userOrder = await UserServices.user.doc(o.idUserOrder).get();
        var userProduct = await UserServices.user.doc(o.idUserProduct).get();
        var product = await ProductServices.product.doc(o.idProduct).get();

        data.add(OrderDataModel(
          id: o.id,
          userOrder: UserModel.fromJson(userOrder.data()),
          userProduct: UserModel.fromJson(userProduct.data()),
          product: ProductModel.snapshot(product),
          status: o.status,
          count: o.count,
          createdAt: o.createdAt,
        ));
      });

      data.sort((a, b) => b.createdAt.compareTo(a.createdAt));

      return data;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future updateStatusOrder(
      String id, String idProduct, int status) async {
    try {
      var ref = order.doc(id);
      if (status < 0) {
        var result = await ProductServices.getProductbyId(idProduct);
        var order = await getOrderByid(id);
        int stock = result.stock + order.count;
        await ProductServices.product.doc(idProduct).update({"stock": stock});
      }
      await ref.update({"status": status});
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future deleteOrder(String id) async {
    try {
      await order.doc(id).delete();
    } on FirebaseException catch (e) {
      throw e;
    }
  }
}
