import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:jastip/Model/chat_model.dart';
import 'package:jastip/Model/chat_user_list_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'collection.dart';

class ChatServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static CollectionReference chat = firestore.collection(Collection.chat);

  static Future<String> chatInitialize(String otherUserId) async {
    String uid1 = AuthService.auth.currentUser.uid;
    String uidChat;

    try {
      //lakuakn check id terlebih dahulu
      var check1 = await chat.doc("$uid1:$otherUserId").get();
      var check2 = await chat.doc("$otherUserId:$uid1").get();

      // check1 ada maka uidChat =  menggunakan id check1
      if (check1.exists) {
        uidChat = check1.id;
      }

      // check1 ada maka uidChat =  menggunakan id check1
      if (check2.exists) {
        uidChat = check2.id;
      }

      if (uidChat == null) {
        uidChat = "$uid1:$otherUserId";
        await chat.doc(uidChat).set({
          "user": [uid1, otherUserId],
          "created_at": Timestamp.fromDate(
            DateTime.now(),
          ),
        });
      }

      return uidChat;
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<ChatModel>> streamMessages(String idChat) {
    try {
      return chat.doc(idChat).collection("message").snapshots().map((event) {
        List<ChatModel> data = [];

        event.docs.forEach((e) {
          ChatModel cht = ChatModel.fromJson(e.data());
          cht.id = e.id;
          data.add(cht);
        });

        data.sort((a, b) {
          return b.time.compareTo(a.time);
        });

        return data;
      });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Stream<List<ChatUserListModel>> getUserMessages(String uid) {
    try {
      return chat.where("user", arrayContains: uid).snapshots().map((c) {
        List<ChatUserListModel> chats = [];
        c.docs.forEach((element) {
          var result = element.data();
          if (result["last_message"] != null) {
            ChatUserListModel data = ChatUserListModel(
              id: element.id,
              user: result["user"].singleWhere((e) => e != uid),
              lastMessage: ChatModel.fromJson(result["last_message"]),
              unread: result["unread"],
            );
            data.id = element.id;
            chats.add(data);
          }
        });

        chats.sort((a, b) => b.lastMessage.time.compareTo(a.lastMessage.time));

        return chats;
      });
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future sendMessage(String id, String msg) async {
    try {
      ChatModel msgValue = ChatModel.fromJson({
        "senderId": AuthService.auth.currentUser.uid,
        "message": msg,
        "status": 0,
        "time": Timestamp.fromDate(DateTime.now()),
      });

      await chat.doc(id).collection("message").add(msgValue.toJson());
      chat.doc(id).update({"last_message": msgValue.toJson()});
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future updateMessageStatus(String id, List<String> idmessages) async {
    await Future.forEach(idmessages, (idmessage) async {
      await readMessage(id, idmessage);
    });
    await unReadMessage(id);
  }

  static Future unReadMessage(String id) async {
    try {
      var data = await chat
          .doc(id)
          .collection("message")
          .where("status", isEqualTo: 0)
          .get();

      Map<String, dynamic> unread = {};

      Map<String, dynamic> updt = {"unread": unread};

      data.docs.forEach((chat) {
        if (unread["${chat["senderId"]}"] == null) {
          unread["${chat["senderId"]}"] = 1;
        } else {
          unread["${chat["senderId"]}"] += 1;
        }
      });

      await chat.doc(id).update(updt);
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future readMessage(String id, String idMessage) async {
    try {
      await chat
          .doc(id)
          .collection("message")
          .doc(idMessage)
          .update({"status": 1});
    } on FirebaseException catch (e) {
      throw e;
    }
  }
}
