import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/collection.dart';

class UserServices {
  static FirebaseFirestore firestore = FirebaseFirestore.instance;
  static FirebaseStorage firebaseStorage = FirebaseStorage.instance;
  static CollectionReference user = firestore.collection(Collection.user);

  static Future updateProfil(
      {String name,
      String phone,
      String address,
      String email,
      File img,
      bool newUser = false}) async {
    try {
      String profilUrl;

      //upload image
      if (img != null) {
        var ref = firebaseStorage.ref(
          Collection.profilFolder(AuthService.auth.currentUser.uid),
        );
        await ref.putFile(img);
        profilUrl = await ref.getDownloadURL();
      }

      if (name != "" && name != null) {
        await AuthService.auth.currentUser.updateProfile(
          displayName: name,
        );
      }

      if (profilUrl != null) {
        await AuthService.auth.currentUser.updateProfile(
          photoURL: profilUrl,
        );
      }

      if (email != "" && email != null) {
        await AuthService.auth.currentUser.updateEmail(email);
      }

      Map<String, dynamic> info = {};

      if (name != "") {
        info["name"] = name;
      }

      if (email != "") {
        info["email"] = email;
      }

      if (profilUrl != "") {
        info["img"] = profilUrl;
      }

      if (phone != "") {
        info["phone"] = phone;
      }

      if (address != "") {
        info["address"] = address;
      }

      if (info.isNotEmpty) {
        if (newUser) {
          await user.doc(AuthService.auth.currentUser.uid).set(info);
        } else {
          await user.doc(AuthService.auth.currentUser.uid).update(info);
        }
      }
    } on FirebaseException catch (e) {
      throw e;
    }
  }

  static Future<UserModel> getUser(String uid) async {
    try {
      var get = await user.doc(uid).get();
      var result = get.data();
      return UserModel(
        id: uid,
        name: result["name"],
        img: result["img"],
        address: result["address"],
        email: result["email"],
        phone: result["phone"],
      );
    } on FirebaseException catch (e) {
      throw e;
    }
  }
}
