import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/firestore/product_services.dart';

class ProfilTokoController extends GetxController {
  final String uid;
  var products = List.filled(0, ProductModel(), growable: true).obs;
  var productisLoad = false.obs;
  var productisEror = false.obs;

  bool get productLoad => productisLoad.value;
  bool get productEror => productisEror.value;

  ProfilTokoController({this.uid});

  @override
  void onInit() {
    super.onInit();
    getProducts(uid);
  }

  Future getProducts(String uid) async {
    try {
      productisLoad(true);
      productisEror(false);
      products.assignAll(await ProductServices.getProductbyUid(uid));
    } on FirebaseException catch (e) {
      print(e.message);
      productisEror(true);
    } finally {
      productisLoad(false);
    }
  }
}
