import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class OrderPageController extends GetxController {
  final loadData = false.obs;
  final isLoaded = false.obs;
  final loadEror = false.obs;
  final statusRefresh = false.obs;
  final listOrder = List.filled(0, OrderDataModel(), growable: true).obs;

  @override
  void onInit() {
    super.onInit();
    listOrder.bindStream(
        OrderServices.streamOrderByUid(AuthService.auth.currentUser.uid)
            .asyncMap((event) {
      if (!isLoaded.value) loadData(true);
      loadEror(false);
      return OrderServices.joinTable(event).then((value) {
        isLoaded(true);
        loadData(false);
        return value;
      }).catchError((e) {
        loadEror(false);
        print(e);
      });
    }));
  }

  Future actionOrder(String id, String idProduct, int status) async {
    try {
      statusRefresh(true);
      isLoaded(false);
      await OrderServices.updateStatusOrder(id, idProduct, status);
      Get.back();
      if (status < 0) {
        Get.snackbar("Berhasil", "Berhasil Membatalkan Orderan");
      } else {
        Get.snackbar(
          "Berhasil",
          "Orderan Telah DiKonfirmasi, Silahkan Chat Untuk Menentukan Kesepakatan Harga Atau COD",
        );
      }
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
      print(e.message);
    } finally {
      statusRefresh(false);
    }
  }
}
