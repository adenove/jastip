import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/category_model.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/config/helper.dart';
import 'package:jastip/core/controller/feed_controller.dart';
import 'package:jastip/core/services/firestore/category_services.dart';
import 'package:jastip/core/services/firestore/product_services.dart';
import 'package:jastip/core/util/util.dart';
import 'package:jastip/ui/screen/Seller/seller_view.dart';

class ProductManagerController extends GetxController {
  final String id;
  final bool fromBuyer;
  File file;
  var product = ProductModel().obs;
  var categories = List.filled(0, CategoryModel(), growable: true).obs;
  var wait = false.obs;
  var waitData = false.obs;

  bool get load => waitData.value;

  ProductManagerController({this.id, this.fromBuyer});

  @override
  void onInit() {
    super.onInit();
    getCategory();
    getProductById(id);
  }

  void changeProfil() async {
    file = File(await Util.getImageFromGallery());
    update();
  }

  void getCategory() async {
    try {
      wait(true);
      List<CategoryModel> data = await CategoryServices.getCategory();
      categories.assignAll(data);

      //Set DropDown Default value
      if (data.length > 0 && product.value.idKategory == null) {
        setCategoryProduct(data[0].id);
      }
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
    } finally {
      wait(false);
    }
  }

  void setCategoryProduct(String idKategory) {
    ProductModel set = ProductModel();
    set.id = product.value.id;
    set.idKategory = idKategory;
    set.img = product.value.img;
    product(set);
  }

  Future getProductById(String idProduct) async {
    if (idProduct != null) {
      try {
        waitData(true);
        product(await ProductServices.getProductbyId(idProduct));
        update();
      } on FirebaseException catch (e) {
        Get.snackbar("eror", e.message);
      } finally {
        waitData(false);
      }
    } else {
      product.value.idCode = Helper.getRandString(8);
      update();
    }
  }

  Future actionProduct(ProductModel product) async {
    try {
      waitData(true);
      await ProductServices.actionProduct(product, file);
      Get.off(() => SellerView());
      if (!fromBuyer) {
        Get.find<FeedController>().getFeed();
      }
    } on FirebaseException catch (e) {
      Get.snackbar("eror", e.message);
    } finally {
      waitData(false);
    }
  }
}
