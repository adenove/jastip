import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/chat_user_list_model.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/chat_services.dart';
import 'package:jastip/core/services/firestore/user_services.dart';

class ChatController extends GetxController {
  var loadData = false.obs;
  var loadEror = false.obs;
  var isLoad = false.obs;
  var userList = List.filled(0, ChatUserListModel(), growable: true).obs;

  @override
  void onInit() {
    super.onInit();
    userList.bindStream(
      ChatServices.getUserMessages(AuthService.auth.currentUser.uid)
          .asyncMap((event) async {
        if (!isLoad.value) loadData(true);
        await Future.forEach(event, (ChatUserListModel e) async {
          loadEror(false);
          try {
            UserModel usr = await UserServices.getUser(e.user).then((value) {
              // loadData(true);
              return value;
            });
            e.dataUser = usr;
          } on FirebaseException catch (e) {
            print(e.message);
            loadEror(true);
          }
        }).then((value) {
          if (!isLoad.value) {
            loadData(false);
            isLoad(true);
          }
        });
        return event;
      }),
    );
  }
}
