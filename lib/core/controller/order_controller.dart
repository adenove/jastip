import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/services/firestore/order_services.dart';
import 'package:jastip/core/services/firestore/user_services.dart';

class OrderController extends GetxController {
  final String uid;
  var loadUser = false.obs;
  var loadUserEror = false.obs;
  var buyWaiting = false.obs;
  var buyIsEror = false.obs;
  var userData = UserModel().obs;
  var countOrder = 1.obs;

  UserModel get user => userData.value;
  bool get userDone => loadUser.value;
  bool get userEror => loadUserEror.value;
  bool get buyWait => buyWaiting.value;
  bool get buyEror => buyIsEror.value;
  int get count => countOrder.value;

  OrderController({this.uid});

  @override
  void onInit() {
    super.onInit();
    getUser(uid);
  }

  Future getUser(String uid) async {
    try {
      loadUser(true);
      loadUserEror(false);
      userData(await UserServices.getUser(uid));
    } on FirebaseException catch (e) {
      loadUserEror(true);
      print(e.message);
    } finally {
      loadUser(false);
    }
  }

  void actionCount(int i) {
    if (count + i > 0) {
      countOrder(count + i);
    }
  }

  Future order(OrderModel order) async {
    try {
      buyWaiting(true);
      buyIsEror(false);
      await OrderServices.addOrder(order);
      Get.back();
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
      buyIsEror(true);
      throw e;
    } finally {
      buyWaiting(false);
      // Get.back();
    }
  }
}
