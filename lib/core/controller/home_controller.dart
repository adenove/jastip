import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/category_model.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/firestore/category_services.dart';
import 'package:jastip/core/services/firestore/product_services.dart';

class HomeController extends GetxController {
  var catrgories = List.filled(0, CategoryModel(), growable: true).obs;
  var products = List.filled(0, ProductModel(), growable: true).obs;
  var loadCategory = false.obs;
  var loadCategoryEror = false.obs;
  var loadProduct = false.obs;
  var loadProductEror = false.obs;
  var idCategoryActive = "".obs;

  String get categoryActive => idCategoryActive.value;

  bool get categoryLoaded => loadCategory.value;
  bool get categoryLoadedEror => loadCategoryEror.value;

  bool get productLoaded => loadProduct.value;
  bool get productLoadedEror => loadProductEror.value;

  @override
  void onInit() {
    super.onInit();
    dataMain();
  }

  Future dataMain() async {
    await getCategory();
    if (catrgories.length > 0) {
      await getProductByIdCategory(catrgories[0].id);
    }
  }

  Future getCategory() async {
    try {
      loadCategory(true);
      loadCategoryEror(false);
      catrgories(await CategoryServices.getCategory());
    } on FirebaseException catch (e) {
      print(e.message);
      loadCategoryEror(true);
    } finally {
      loadCategory(false);
    }
  }

  Future getProductByIdCategory(String idCategory) async {
    try {
      loadProduct(true);
      loadProductEror(false);
      idCategoryActive(idCategory);
      products(await ProductServices.getProductbyIdCategory(idCategory));
    } on FirebaseException catch (e) {
      print(e.message);
      loadProductEror(true);
    } finally {
      loadProduct(false);
    }
  }

  void search(String value) async {
    try {
      loadProduct(true);
      loadProductEror(false);
      List<ProductModel> data =
          await ProductServices.getProductbyIdCategory(categoryActive);
      List<ProductModel> found = [];
      data.forEach((e) {
        if (e.name.toUpperCase().indexOf(value.toUpperCase()) > -1) {
          found.add(e);
        }
      });
      products(found);
    } on FirebaseException catch (e) {
      print(e.message);
      loadProductEror(true);
    } finally {
      loadProduct(false);
    }
  }
}
