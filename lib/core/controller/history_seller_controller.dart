import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class HistorySellerController extends GetxController {
  final historyLoad = false.obs;
  final historyLoadEror = false.obs;
  final historys = List.filled(0, OrderDataModel(), growable: true).obs;

  @override
  void onInit() {
    super.onInit();
    historys.bindStream(
        OrderServices.streamHistoryByUid(AuthService.auth.currentUser.uid)
            .asyncMap((event) {
      historyLoad(true);
      historyLoadEror(false);
      return OrderServices.joinTable(event).then((value) {
        historyLoad(false);
        return value;
      }).catchError((e) {
        historyLoadEror(true);
        print(e);
      });
    }));
  }
}
