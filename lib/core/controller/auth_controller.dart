import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/user_services.dart';
import 'package:jastip/ui/screen/Home/Home_view.dart';
import 'package:jastip/ui/screen/root_view.dart';

class AuthController extends GetxController {
  final scured = true.obs;
  final isWait = false.obs;

  void login(String email, String password) async {
    isWait(true);
    try {
      await AuthService.login(email, password);
      Get.offAll(() => RootView());
    } on FirebaseAuthException catch (e) {
      Get.snackbar("Eror", e.message);
    } finally {
      isWait(false);
    }
  }

  void signup(String email, String password, String phone) async {
    isWait(true);
    try {
      await AuthService.signUp(email, password);
      if (AuthService.auth.currentUser != null) {
        await UserServices.updateProfil(
          email: email,
          phone: phone,
          newUser: true,
        );
        Get.offAll(() => HomeView());
      }
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
    } finally {
      isWait(false);
    }
  }
}
