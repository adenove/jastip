import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/chat_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/chat_services.dart';

class ChatDetailController extends GetxController {
  final loadIdChat = false.obs;
  final loadIdEror = false.obs;
  final loadChat = false.obs;
  final loadErorChat = false.obs;

  final String uid;
  String chatId;

  var chats = List.filled(0, ChatModel(), growable: true).obs;

  ChatDetailController({this.uid});

  @override
  void onInit() {
    super.onInit();
    initializedChat(uid);
  }

  Future initializedChat(String uid) async {
    try {
      loadIdChat(true);
      loadIdEror(false);
      chatId = await ChatServices.chatInitialize(uid);
      chats.bindStream(ChatServices.streamMessages(chatId));
    } on FirebaseException catch (e) {
      loadErorChat(false);
      throw e;
    } finally {
      loadIdChat(false);
    }
  }

  void sendMessage(String message) async {
    try {
      await ChatServices.sendMessage(chatId, message);
      checkMessage();
    } on FirebaseException catch (e) {
      print(e.message);
      throw e;
    } finally {
      // print("gagal");
    }
  }

  void checkMessage() async {
    try {
      List<String> idMessages = [];

      chats.forEach((msg) {
        if (msg.status == 0 &&
            msg.senderId != AuthService.auth.currentUser.uid) {
          idMessages.add(msg.id);
        }
      });

      ChatServices.updateMessageStatus(chatId, idMessages);
      // getListChat(chatId);
    } on FirebaseException catch (e) {
      print(e.message);
      throw e;
    } finally {
      // print("gagal");
    }
  }
}
