import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/order_data_model.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class OrderSellerController extends GetxController {
  final orderRefresh = false.obs;
  final orderIsload = false.obs;
  final orderEror = false.obs;
  final statusRefresh = false.obs;
  final orders = List.filled(0, OrderDataModel(), growable: true).obs;
  var selects = [].obs;

  @override
  void onInit() {
    super.onInit();
    orders.bindStream(
      OrderServices.streamOrderRequestByUid(AuthService.auth.currentUser.uid)
          .asyncMap((List<OrderModel> event) async {
        if (!orderIsload.value) orderRefresh(true);
        orderEror(false);
        return OrderServices.joinTable(event).then((value) {
          if (!orderIsload.value) orderRefresh(false);
          orderIsload(true);
          return value;
        }).catchError((e) {
          print(e);
          orderEror(true);
        });
      }),
    );
  }

  Future getOrder(String uid) async {
    try {
      orderRefresh(true);
      orderEror(false);
      orders.assignAll(await OrderServices.getSellerOrderByUid(uid));
    } on FirebaseException catch (e) {
      orderEror(true);
      print(e.message);
    } finally {
      orderRefresh(false);
    }
  }

  Future actionOrder(String id, String idProduct, int status) async {
    try {
      statusRefresh(true);
      orderIsload(false);
      await OrderServices.updateStatusOrder(id, idProduct, status);
      Get.back();
      if (status < 0) {
        Get.snackbar("Berhasil", "Berhasil Membatalkan Orderan");
      } else {
        Get.snackbar(
          "Berhasil",
          "Orderan Telah DiKonfirmasi, Silahkan Chat Untuk Menentukan Kesepakatan Harga Atau COD",
        );
      }
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
      print(e.message);
    } finally {
      statusRefresh(false);
    }
  }

  void longPress(String id) {
    if (!selects.contains(id)) {
      selects.add(id);
    }
  }

  void deleteOrder() async {
    orderIsload(false);
    await Future.forEach(selects, (e) async {
      await OrderServices.deleteOrder(e);
    }).then((value) {
      selects.clear();
    }).catchError((e) {
      print(e);
    });
  }
}
