import 'dart:io';
import 'package:get/get.dart';
import 'package:jastip/core/util/util.dart';

class SettingController extends GetxController {
  File fileProfil;

  void getImage() async {
    fileProfil = File(await Util.getImageFromGallery());
    update();
  }
}
