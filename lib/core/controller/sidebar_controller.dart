import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/chat_user_list_model.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/chat_services.dart';
import 'package:jastip/core/services/firestore/user_services.dart';

class SidebarController extends GetxController {
  var userData = UserModel().obs;
  var isWait = false.obs;
  var userWait = false.obs;
  var countChat = 0.obs;

  bool get wait => isWait.value;
  bool get userDone => userWait.value;

  UserModel get user => userData.value;

  @override
  void onInit() {
    super.onInit();
    getUser();
    countChat.bindStream(
      ChatServices.getUserMessages(AuthService.auth.currentUser.uid).map((e) {
        int count = 0;
        e.forEach((ChatUserListModel c) {
          if (c.unread["${c.user}"] != null) {
            count += c.unread["${c.user}"];
          }
        });
        return count;
      }),
    );
  }

  void getUser() async {
    try {
      userWait(true);
      userData(await UserServices.getUser(AuthService.auth.currentUser.uid));
    } on FirebaseException catch (e) {
      print(e.message);
    } finally {
      userWait(false);
    }
  }
}
