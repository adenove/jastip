import 'package:get/get.dart';
import 'package:jastip/Model/order_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/order_services.dart';

class SellerController extends GetxController {
  int menuIndex = 0;
  final order = 0.obs;

  @override
  void onInit() {
    super.onInit();
    order.bindStream(
      OrderServices.streamOrderRequestByUid(AuthService.auth.currentUser.uid)
          .map((List<OrderModel> event) {
        return event.length;
      }),
    );
  }

  void setMenuIndex(int i) {
    menuIndex = i;
    update();
  }
}
