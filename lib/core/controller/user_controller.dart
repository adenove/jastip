import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/user_model.dart';
import 'package:jastip/core/controller/sidebar_controller.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/user_services.dart';
import 'package:jastip/core/util/util.dart';

class UserController extends GetxController {
  var userData = UserModel().obs;
  var isWait = false.obs;
  var userWait = false.obs;
  File fileProfil;

  bool get wait => isWait.value;
  bool get userDone => userWait.value;

  UserModel get user => userData.value;

  @override
  void onInit() {
    super.onInit();
    getUser();
  }

  void getUser() async {
    try {
      userWait(true);
      userData(await UserServices.getUser(AuthService.auth.currentUser.uid));
      Get.find<SidebarController>().getUser();
      update();
    } on FirebaseException catch (e) {
      print(e.message);
    } finally {
      userWait(false);
    }
  }

  void updateProfil({
    String name,
    String email,
    String phone,
    String address,
    File img,
  }) async {
    try {
      isWait(true);
      await UserServices.updateProfil(
        name: name,
        email: email,
        phone: phone,
        address: address,
        img: img,
      );
      getUser();
      Get.snackbar("Info", "Profil Berhasil di update");
    } on FirebaseException catch (e) {
      Get.snackbar("Eror", e.message);
    } finally {
      isWait(false);
    }
  }

  void getImage() async {
    fileProfil = File(await Util.getImageFromGallery());
    update();
  }
}
