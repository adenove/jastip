import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jastip/Model/product_model.dart';
import 'package:jastip/core/services/auth_service.dart';
import 'package:jastip/core/services/firestore/product_services.dart';
import 'package:jastip/ui/screen/Seller/Page/product_manager_view.dart';

class FeedController extends GetxController {
  var feed = List.filled(0, ProductModel(), growable: true).obs;
  var feedLoad = false.obs;
  var feedIsEror = false.obs;
  final selectsFeed = [].obs;

  get feedWait => feedLoad.value;
  get feedEror => feedIsEror.value;

  @override
  void onInit() {
    super.onInit();
    getFeed();

    feed.bindStream(
      ProductServices.streamProductbyUid(AuthService.auth.currentUser.uid),
    );
  }

  void getFeed() async {
    try {
      feedLoad(true);
      feedIsEror(false);

      feed(await ProductServices.getProductbyUid(
          AuthService.auth.currentUser.uid));
    } on FirebaseException catch (e) {
      print(e.message);
      feedIsEror(true);
    } finally {
      feedLoad(false);
    }
  }

  void edit(String id) {
    // if (selectsFeed.length > 0) {
    //   if (selectsFeed.contains(id)) {
    //     selectsFeed.remove(id);
    //   } else {
    //     selectsFeed.add(id);
    //   }

    //   return;
    // }

    Get.to(
      () => ProductManagerView(
        idProduct: id,
        title: "Edit Product",
      ),
    );
  }

  void longPress(String id) {
    if (!selectsFeed.contains(id)) {
      selectsFeed.add(id);
    }
  }

  void delete(String id) async {
    // if (!feedLoad.value) {
    //   feedLoad(true);
    //   print(selectsFeed);
    //   await Future.forEach(selectsFeed, (id) async {
    //     await ProductServices.deleteProduct(id).then((value) {
    //       selectsFeed.remove(id);
    //     }).catchError((e) {
    //       print("no-img");
    //     });
    //   });

    //   getFeed();
    // }

    Get.defaultDialog(
      title: "Delete Product",
      content: Padding(
        padding: const EdgeInsets.all(20),
        child: Text(
          "Anda Yakin Ingin Menghapus Product Ini?",
          textAlign: TextAlign.center,
        ),
      ),
      actions: [
        ElevatedButton(
          onPressed: () async {
            try {
              Get.back();
              await ProductServices.deleteProduct(id);
            } on FirebaseException catch (e) {
              print("${e.message}");
            } finally {
              getFeed();
            }
          },
          child: Text("Delete"),
          style: ElevatedButton.styleFrom(
            primary: Colors.red,
          ),
        ),
        ElevatedButton(
          onPressed: () => Get.back(),
          child: Text("Batal"),
          style: ElevatedButton.styleFrom(
            primary: Colors.grey,
          ),
        ),
      ],
    );
  }
}
