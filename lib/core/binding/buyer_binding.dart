import 'package:get/get.dart';
import 'package:jastip/core/controller/home_controller.dart';

class BuyerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
  }
}
