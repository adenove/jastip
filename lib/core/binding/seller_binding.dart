import 'package:get/get.dart';
// import 'package:jastip/core/controller/feed_controller.dart';
import 'package:jastip/core/controller/seller_controller.dart';
// import 'package:jastip/core/controller/user_controller.dart';

class SellerBinding implements Bindings {
  @override
  void dependencies() {
    // Get.lazyPut(() => FeedController());
    Get.lazyPut(() => SellerController());
  }
}
